from functools import wraps
from cqhttp import CQHttp as _CQHttp
from typing import Callable, Iterable

class CQHttp(_CQHttp):
    def set_handler(self, post_type: str,  func: Callable, types: Iterable=None):
        """
        设置回调处理函数

        --------------

        通常可以用 **@self.on_message** 、 **@self.on_request** 、 **@self.on_event** 这几个装饰器来设定回调函数，但在不允许使用装饰器的情况下，可以使用本方法设定回调函数。
        每个上报类型的每个特定信息类型只能设置一个处理函数。

        --------------

        :param str post_type: 上报类型，以下三者其一："message"、"request"、"event"
        :param Callable func: 指定处理函数
        :param Iterable | None types: 信息类型。详情参见API文档中的消息与事件类型。默认为空，为空时注册该函数处理全部类型。

        :return: None
        """
        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        if types:
            for t in types:
                self._handlers[post_type][t] = wrapper
        else:
            self._handlers[post_type]['*'] = wrapper

    def unset_handler(self, post_type: str, types: Iterable = None):
        """
        删除回调处理函数

        --------------

        解除注册指定上报类型的回调函数。
        如果指定了信息类型，则只会解除所请求的信息类型的回调函数的注册。

        **注意**: 如果要单独解除默认监听全部信息类型的回调函数的注册，请指定 **['*']** 。如果不指定事件类型，将会解除整个上报类型的全部回调函数的注册。

        --------------

        :param str post_type: 上报类型，以下三者其一："message"、"request"、"event"
        :param Iterable | None types: 信息类型。详情参见API文档中的消息与事件类型。默认为空，为空时解除整个上报类型的全部回调函数的注册。
        :return: None
        """

        if types:
            for t in types:
                self._handlers[post_type].pop(t)
            if len(self._handlers[post_type]) == 0:
                self._handlers.pop(post_type)
        else:
            self._handlers.pop(post_type)

    def sdk_local_stop(self):
        pass
