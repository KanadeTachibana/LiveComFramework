"""
LiveCom Unified API Caller module.
"""
# LiveCom Unified API callers
from LiveCom.SystemComponents.EventBus_Demo import EventBus
from typing import Callable
from threading import Lock


class EType(object):
    """
    EType: EventType, indicate two of the queues.

    INCOMING for incoming receivers, such as message processor;
    OUTGOING for outgoing senders, such as messageing adaptors.
    """
    INCOMING = "incoming"
    OUTGOING = "outgoing"


class APIManagerError(Exception):
    pass


class GroupNotFoundError(Exception):
    def __init__(self, group_etype: str, group_name, target_type: str=None):
        self.target_type = target_type
        self.group_etype = group_etype
        self.group_name = group_name

    def __str__(self):
        if self.target_type is None:
            return "Requested group not found: EType: '" + \
                   self.group_etype + "', Name: '" + self.group_name + "'."
        return "Requested " + self.target_type + " target not found: EType: '" + \
            self.group_etype + "', Name: '" + self.group_name + "'."


class DuplicatedBroadcastReceiverError(Exception):
    def __init__(self, cast_name: str):
        self.cast_name = cast_name

    def __str__(self):
        return "Duplicated broadcast receiver name: " + self.cast_name


class DuplicatedBroadcastConnectorError(Exception):
    def __init__(self, sender_name: str):
        self.sender_name = sender_name

    def __str__(self):
        return "Duplicated broadcast receiver name: " + self.sender_name


class DuplicatedLibraryError(Exception):
    def __init__(self, library_name: str):
        self.library_name = library_name

    def __str__(self):
        return "Duplicated library name: " + self.library_name


class DuplicatedFunctionError(Exception):
    def __init__(self, function_name: str, library_name: str):
        self.function_name = function_name
        self.library_name = library_name

    def __str__(self):
        return "Duplicated function in library '" + self.library_name + "': " + self.function_name


class NoSuchLibraryError(Exception):
    def __init__(self, library_name: str):
        self.library_name = library_name

    def __str__(self):
        return "No such library '" + self.library_name + "'."


class NoSuchFunctionError(Exception):
    def __init__(self, function_name: str, library_name: str):
        self.function_name = function_name
        self.library_name = library_name

    def __str__(self):
        return "No such function '" + self.function_name + "' in library '" + self.library_name + "'."


class DestroyLibraryInUse(Exception):
    def __init__(self, library_name: str, remaining_ref_count: int):
        self.library_name = library_name
        self.remaining_ref_count = remaining_ref_count

    def __str__(self):
        return "Destroying in-use library '" + self.library_name + \
               "', library reference count is not zero. (Remaining reference(s): " + str(self.remaining_ref_count) + ")"


class APIManager(object):
    # service type: outgoing
    # service info: provide API registration information
    # this is a global wise object and should only create once in LiveCom FW.
    # API registration information is only used to locate a API "Library".
    # e.g.: Get response / request etype and listen groups.
    def __init__(self, sysbus: EventBus):
        # register library registration information
        # base format:
        # example_group_ref_table = {
        #     (etype, group_name): [create_count, refernce_count]
        # }
        self._group_reference_table = {}
        # TODO: rewrite here to implement caller reuse.
        self._apihost_info_dataset = {}
        # { library_name: [req_etype, req_name, resp_etype, resp_name, ..?]}
        # self._apiclient_storage = {}
        self._sysbus = sysbus
        # define lock manager
        self.APILoader = APILoader(api_manager = self, sysbus = sysbus)
        # lock_tokens = { "token_name": {"return": return_values} or {"exception": exception_obj}, ... }

        # broadcast related objects:

        self._broadcast_receiver_object = dict()
        self._broadcast_connector_object = dict()
        # _broadcast_refs = { (listen_etype, listen_group): [listens, connects]}
        self._broadcast_refs = dict()
        pass

    def create_library(self, libname: str, request_etype: str, request_group: str,
                       response_etype: str, response_group: str):
        return _APIHost(self._sysbus, "", "", "",
                        "", libname=libname, api_manager=self)
        pass

    def _destroy_library(self, libname: str, *, called_by_worker: bool=False,
                         ____self_destroy____: bool=False):
        pass

    def load_library(self, libname: str, *, client_name=None):
        return _APIClient(sysbus=self._sysbus, req_etype="", req_group="",
                          resp_etype="", resp_group="",
                          client_name=client_name, libname=libname, api_manager=self)
        pass

    def _unload_library(self, libname: str, *, called_by_worker=False,
                        ____self_destroy____: bool=False):
         pass

    def _check_group_add_required(self, listen_etype: str, listen_group: str):
        return False

    def _check_group_remove_required(self, listen_etype: str, listen_group: str):
        return False
        pass

    def _add_broadcast_receiver_ref(self, listen_etype: str, listen_group: str):
        pass

    def _remove_broadcast_receiver_ref(self, listen_etype: str, listen_group: str):
        pass

    def _add_broadcast_connector_ref(self, listen_etype: str, listen_group: str):
        pass

    def _remove_broadcast_connector_ref(self, listen_etype: str, listen_group: str):
        pass

    def _create_broadcast_group(self, listen_etype: str, listen_group: str):
        pass

    def _remove_broadcast_group(self, listen_etype: str, listen_group: str):
        pass

    def create_broadcast_receiver(self, listener_group_etype: str,
                                  listener_group_name: str, cast_name: str):
        APIReceiver = _APIBroadcastReceiver(cast_name=cast_name,
                                            listener_group_etype=listener_group_etype,
                                            listener_group_name=listener_group_name,
                                            api_manager=self)
        return APIReceiver
        pass

    def _destroy_broadcast_receiver(self, *, cast_name: str,
                                    listener_group_etype: str,
                                    listener_group_name: str,
                                    called_inside=False):
        pass

    def create_brodacast_connecter_by_cast_name(self, sender_name: str, cast_name: str = None):
        return self.create_brodacast_connecter(sender_name=sender_name,
                                               listener_group_name="",
                                               listener_group_etype="")
        pass

    def create_brodacast_connecter(self, sender_name: str, source_module_name: str,
                                   listener_group_etype: str,
                                   listener_group_name: str, *,
                                   allow_invaild_target: bool=True):
        APISender = _APIBroadcastConnector(sender_name=sender_name,
                                           listener_group_etype=listener_group_etype,
                                           listener_group_name=listener_group_name,
                                           api_manager=self,
                                           allow_invaild_target=allow_invaild_target)

        return APISender
        pass

    def _destroy_broadcast_connector(self, *, sender_name: str,
                                     listener_group_etype: str,
                                     listener_group_name: str,
                                     called_inside=False):
        pass


class _APIBroadcastReceiver(object):
    def __init__(self, cast_name: str, listener_group_etype: str,
                 listener_group_name: str, *, api_manager: APIManager):
        self._reg_func_info = dict()
        self._lst_group_etype = listener_group_etype
        self._lst_group_name = listener_group_name
        self._cast_name = cast_name
        self._APIManager = api_manager
        # register executable here.
        self._available = True
        pass

    def _setup_listener(self):
        pass

    def _takedown_listener(self):
        pass

    def _distribute_worker(self, api_context: dict, metadata: dict):
        pass

    def register(self, func_name: str, handler: Callable, *, metadata_kwarg_name: str=None,
                 append_kwargs: dict=None):
        pass

    def unregister(self, func_name: str):
        pass

    def __del__(self):
        pass


class _APIBroadcastConnector(object):
    def __init__(self, sender_name: str, source_module_name: str,
                 listener_group_etype: str, listener_group_name: str,
                 *,  allow_invaild_target: bool=True,
                 api_manager: APIManager):
        self._reg_func_info = dict()
        self._lst_group_etype = listener_group_etype
        self._lst_group_name = listener_group_name
        self._sender_name = sender_name
        self._APIManager = api_manager
        self.allow_invaild_target = allow_invaild_target
        self._available = True

    def call(self, args, kwargs):
        pass

    def __call__(self, *args, **kwargs):
        pass

    def __del__(self):
        pass


class _APIHost(object):
    def __init__(self, sysbus: EventBus, req_etype: str, req_group: str, resp_etype: str,
                 resp_group: str, *, libname: str=None, api_manager: APIManager):
        self._callers = {}
        self.libname = libname
        self._available = True
        self._req_etype = req_etype
        self._req_group = req_group
        self._resp_etype = resp_etype
        self._resp_group = resp_group
        pass
        self._APIManager = api_manager
        self._sysbus = sysbus
        pass

    def _req_worker(self, context, metadata):
        pass

    def register(self, function_name: str, handler: Callable, *, metadata_kwarg_name: str = None,
                 append_kwargs: dict=None):
        pass

    def unregister(self, function_name: str):
        pass

    def _unregister_all(self):
        pass

    def get_function_list(self):
        return list(self._callers.keys())

    def ____self_destroy____(self, *, __confirm__=False):
        pass

    def __del__(self):
        pass

    def __repr__(self):
        if self._available:
            return "<_APIHost object: '" + self.libname + "' at " + str(hex(id(self)))+">"
        else:
            return "<[UNAVAILABLE] _APIHost object: '" + self.libname + "' at " + str(hex(id(self))) + ">"
    pass


class _APIClient(object):
    def __init__(self, sysbus: EventBus, req_etype: str, req_group: str, resp_etype: str,
                 resp_group: str, client_name: str, *, libname: str, api_manager: APIManager):
        self._lock_container = dict()
        self.libname = libname
        self._req_etype = req_etype
        self._req_group = req_group
        self._resp_etype = resp_etype
        self._resp_group = resp_group
        self._client_name = client_name
        self._APIManager = api_manager
        self._sysbus = sysbus
        self._function_list = []
        self._available = True
        pass

    def _create_lock_object(self):
        lock = Lock()
        lock_token = 0
        return lock_token, lock

    def call_api(self, function_name, args, kwargs):
        pass

    def _resp_worker(self, context: dict, metadata: dict):
        pass

    def get_function_list(self, update=False):
        return self._function_list
        pass

    def _get_function_list_refresh(self, aasync=False):
        pass

    def check_function_availablity(self, function_name):
        return False
        pass

    def spawn_caller_module(self, active_function_sync: bool=False, *, ignore_api_check: bool=False):
        return APICallerModule(self,
                               always_update_when_check=bool(active_function_sync),
                               ignore_api_check=bool(ignore_api_check)
                               )

    def ____self_destroy____(self, *, __confirm__=False):
            pass

    def __del__(self):
        if not self._sysbus.is_running():
            self.____self_destroy____(__confirm__=True)
            return
        print("Requesting unload library: " + self.libname)
        self._APIManager._unload_library(self.libname, called_by_worker=True)
        pass

    def __repr__(self):
        return "<_APIClient object: '" + self.libname + "' at " + str(hex(id(self)))+">"
    pass


class APICallerModule(object):
    def __init__(self, client_obj: _APIClient, *,
                 call_target: str=None, ignore_api_check: bool=False,
                 always_update_when_check: bool=False):
        # just use the reference to call something.
        self._client_obj = client_obj
        self._call_target = call_target
        self._ignore_api_check = ignore_api_check
        self.always_update_when_check = always_update_when_check
        pass

    def caller_module_type(self):
            return "library"

    # def __getattr__(self, item):
    #     if self._call_target is not None:
    #         if item == "__call__":
    #             return self
    #         else:
    #             return super().__getattribute__(item)

    #     # query
    #     if self._ignore_api_check:
    #         return APICallerModule(self._client_obj, call_target=item,
    #                                ignore_api_check=True)

    #     # check availability
    #     if item in self._client_obj.get_function_list(
    #             update=self.always_update_when_check):
    #         return APICallerModule(self._client_obj, call_target=item,
    #                                ignore_api_check=False)
    #     # finalize
    #     return super().__getattribute__(item)
    #     pass

    def __call__(self, *args, **kwargs):
        if self._call_target is not None:
            return self._client_obj.call_api(self._call_target, args, kwargs)
        else:
            raise TypeError("'APICallerModule Library' object is not callable")
        pass

    # def __dir__(self):
    #     container = ["caller_module_type"]
    #     container.extend(super().__dir__())

    #     if self._call_target is not None:
    #         container.append("__call__")
    #         return container

    #     if not self._ignore_api_check:
    #         container.extend(
    #             self._client_obj.get_function_list(self.always_update_when_check)
    #         )
    #     return container

    def __repr__(self):
        if self._call_target is not None:
            return "<APICallerModule Function '" + \
                self._call_target + "' of Library '" + \
                   self._client_obj.libname + "' at " + str(hex(id(self))) + ">"
        else:
            # library object
            return "<APICallerModule Library '" + \
                   self._client_obj.libname + "' at " + str(hex(id(self))) + ">"
            pass
        pass


class ModuleDependencyError(Exception):
    # case Version mismatch: ("version", (expecting, got))
    # case No available module: ("no_available", (module_name, module_version))
    pass


class ModuleFormatError(Exception):
    pass


class APILoader(object):
    def __init__(self, api_manager: APIManager, sysbus: EventBus):
        self._api_manager = api_manager
        self._sysbus = sysbus
        # global operation lock
        self._loader_lock = Lock()
        self._available_module_table = dict()  # store bare module object
        self._active_module_table = dict()  # store bare module object
        self._module_dep_table = dict()  # store module depend info
        self._module_ref_table = dict()  # store module reference info
        self._module_direct_ref_count = dict()  # store module direct reference count
        self._system_operation_lock = Lock()
        self._system_operation_lock.acquire()
        self._refresh_module_table()
        self._system_operation_lock.release()

    def ____acquire_control____(self):
        pass

    def ____release_control____(self):
        pass

    def ____wait_system_lock____(self):
        pass

    def _refresh_module_table(self, *, ignore_error=False):
        pass

    def _system_startup(self):
        pass

    def _system_shutdown(self):
        pass

    def _dep_load_probe(self, module_name, module_version=None):
        # version = None means any.
        deptable = []
        return deptable
        pass

    def _check_load_dep(self, mod_name: str):
        load_seq = []
        return load_seq
        pass

    def _check_unload_dep(self, module_name):
        load_seq = []
        return load_seq

    def _dep_unload_probe(self, module_name: str):
        deptable = []
        return deptable
        pass

    def load_module(self, mod_name: str):
        pass

    def _load_module(self, mod_name: str):
        pass

    def _mod_setup(self, mod_name: str):
        pass

    def unload_module(self, mod_name: str):
        pass

    def _unload_module(self, mod_name: str):
        pass

    def _mod_takedown(self, mod_name: str):
        pass


class DuplicatedUMPObjectError(Exception):
    def __init__(self, module_name: str):
        self.module_name = module_name

    def __str__(self):
        return "Duplicated UMP object for module '" \
               + self.module_name + \
               "', every module should only have one UMP object."
