"""
LiveCom Unified API Caller module.
"""
# LiveCom Unified API callers
from LiveCom.SystemComponents.EventBus_Demo import EventBus
import LiveCom.Config.LoadConfig as ConfigManager
from LiveCom.Auxiliaries.LiveCom.LoadAdaptors import Probe  # AdaptorLoadError
from LiveCom.Auxiliaries.LiveCom.CommonExcptions import NotAvailableError
from typing import Callable
import sys
from random import randint
from threading import Lock
from collections import defaultdict
from LiveCom.Auxiliaries.LiveCom.API.APICommonExceptions import *
from LiveCom.Auxiliaries.LiveCom.EventBus.EventBusToolkits import EType


class APIManager(object):
    # service type: outgoing
    # service info: provide API registration information
    # this is a global wise object and should only create once in LiveCom FW.
    # API registration information is only used to locate a API "Library".
    # e.g.: Get response / request etype and listen groups.
    def __init__(self, sysbus: EventBus):
        # register library registration information
        # base format:
        # example_group_ref_table = {
        #     (etype, group_name): [create_count, refernce_count]
        # }
        self._group_reference_table = {}
        # TODO: rewrite here to implement caller reuse.
        self._apihost_info_dataset = {}
        # { library_name: [req_etype, req_name, resp_etype, resp_name, ..?]}
        # self._apiclient_storage = {}
        self._sysbus = sysbus
        # define lock manager
        self.APILoader = APILoader(api_manager = self, sysbus = sysbus)
        # lock_tokens = { "token_name": {"return": return_values} or {"exception": exception_obj}, ... }

        # broadcast related objects:

        self._broadcast_receiver_object = dict()
        self._broadcast_connector_object = dict()
        # _broadcast_refs = { (listen_etype, listen_group): [listens, connects]}
        self._broadcast_refs = dict()

        self.user_extra_data = defaultdict(dict)

        pass

    def create_library(self, libname: str, request_etype: str, request_group: str,
                       response_etype: str, response_group: str):
        # check libname duplicate
        if libname in self._apihost_info_dataset.keys():
            raise DuplicatedLibraryError(libname)
            # APIManagerError("Duplicate library libname.", libname)

        # no duplicate.
        # check group refernce.
        # Step 1: Request Types check

        # does have that key?
        if (request_etype, request_group) in self._group_reference_table:
            self._group_reference_table[(request_etype, request_group)][0] += 1
        else:
            self._group_reference_table[(request_etype, request_group)] = [1, 0]

        # Step 2: Response Types check
        if (response_etype, response_group) in self._group_reference_table:
            self._group_reference_table[(response_etype, response_group)][1] += 1
        else:
            self._group_reference_table[(response_etype, response_group)] = [0, 1]

        self._apihost_info_dataset[libname] = [
            request_etype,
            request_group,
            response_etype,
            response_group,
            0  # referenced_count
        ]

        # seems we have registered reference table.
        # create listen group bening used.
        self._sysbus.create_group(request_etype, request_group, ignore_existed=True)
        self._sysbus.create_group(response_etype, response_group, ignore_existed=True)

        # return actual object
        return _APIHost(self._sysbus, request_etype, request_group, response_etype,
                        response_group, libname=libname, api_manager=self)
        pass

    def _destroy_library(self, libname: str, *, called_by_worker: bool=False,
                         ____self_destroy____: bool=False):
        if called_by_worker:
            # unregister API host

            # check libname availability.
            if libname not in self._apihost_info_dataset.keys():
                raise NoSuchLibraryError(libname)
                # APIManagerError("No such library.", libname)

            request_etype = self._apihost_info_dataset[libname][0]
            request_group = self._apihost_info_dataset[libname][1]
            response_etype = self._apihost_info_dataset[libname][2]
            response_group = self._apihost_info_dataset[libname][3]
            referenced_count = self._apihost_info_dataset[libname][4]

            if not ____self_destroy____ or self._sysbus.is_running():
                if referenced_count > 0:
                    # this should not allowed to happen.
                    raise DestroyLibraryInUse(libname, referenced_count)
                    # APIManagerError("Destroying library in use. (Reference count is not zero)",
                    #                      libname, referenced_count)
            # check request object
            if (request_etype, request_group) not in self._group_reference_table:
                raise GroupNotFoundError(request_etype, request_group, "request")
                # APIManagerError("Requested request target not found.", request_etype, request_group)
            # check response object
            if (response_etype, response_group) not in self._group_reference_table:
                raise GroupNotFoundError(response_etype, response_group, "response")
                # APIManagerError("Requested response target not found.", response_etype, response_group)

            if self._group_reference_table[(request_etype, request_group)][0] <= 0:
                if self._group_reference_table[(request_etype, request_group)][0] != 0:
                    print("APIManager: _destroy_library: WARNING: Request target reference is less than zero!")
                    self._group_reference_table[(request_etype, request_group)][0] = 0
                    # fix error
                raise APIManagerError("Requested request target is not referenced.")
            if self._group_reference_table[(response_etype, response_group)][1] <= 0:
                if self._group_reference_table[(response_etype, response_group)][1] != 0:
                    print("APIManager: _destroy_library: WARNING: Response target reference is less than zero!")
                    self._group_reference_table[(response_etype, response_group)][1] = 0
                    # fix error
                raise APIManagerError("Requested response target is not referenced.")

            # everything is set
            self._group_reference_table[(request_etype, request_group)][0] -= 1
            self._group_reference_table[(response_etype, response_group)][1] -= 1

            # do not remove anything while system bus is stopped.
            if not ____self_destroy____ or self._sysbus.is_running():
                print("removing library: " + libname)
                self._apihost_info_dataset.pop(libname)
            pass

        else:
            print("APIManager: _destroy_library: INFO: Ignored _destroy_library due to not implemented.")
        pass

    def load_library(self, libname: str, *, client_name=None):
        # an library loader will register
        # check libname duplicate
        if libname not in self._apihost_info_dataset.keys():
            raise NoSuchLibraryError(libname)
            # APIManagerError("Requested library not found.", libname)

        request_etype = self._apihost_info_dataset[libname][0]
        request_group = self._apihost_info_dataset[libname][1]
        response_etype = self._apihost_info_dataset[libname][2]
        response_group = self._apihost_info_dataset[libname][3]
        # referenced_count = self.apihost_info_dataset[libname][4]

        # check request object
        if (request_etype, request_group) not in self._group_reference_table:
            raise GroupNotFoundError(request_etype, request_group, "request")
            # APIManagerError("Requested request target not found.", request_etype, request_group)
        # check response object
        if (response_etype, response_group) not in self._group_reference_table:
            raise GroupNotFoundError(response_etype, response_group, "response")
            # APIManagerError("Requested response target not found.", response_etype, response_group)

        if self._group_reference_table[(request_etype, request_group)][0] <= 0:
            if self._group_reference_table[(request_etype, request_group)][0] != 0:
                print("APIManager: _load_library: WARNING: Request target reference is less than zero!")
                self._group_reference_table[(request_etype, request_group)][0] = 0  # fix error
            raise APIManagerError("Requested request target is not referenced.")
        if self._group_reference_table[(response_etype, response_group)][1] <= 0:
            if self._group_reference_table[(response_etype, response_group)][1] != 0:
                print("APIManager: _load_library: WARNING: Response target reference is less than zero!")
                self._group_reference_table[(response_etype, response_group)][1] = 0  # fix error
            raise APIManagerError("Requested response target is not referenced.")

        # everything has been checked.
        # things to be done:
        # 1. register library usage by library name
        # 3. create target object

        # 1.
        self._apihost_info_dataset[libname][4] += 1  # referenced_count
        self._group_reference_table[(request_etype, request_group)][1] += 1
        self._group_reference_table[(response_etype, response_group)][0] += 1

        # how we know there is a client name available?
        if client_name is None:
            # this will locate the package name of caller.
            client_name = sys._getframe().f_back.f_globals["__name__"]
            # print(">>>" + client_name)

        # create listen group bening used.
        self._sysbus.create_group(request_etype, request_group, ignore_existed=True)
        self._sysbus.create_group(response_etype, response_group, ignore_existed=True)

        # 2.
        return _APIClient(sysbus=self._sysbus, req_etype=request_etype, req_group=request_group,
                          resp_etype=response_etype, resp_group=response_group,
                          client_name=client_name, libname=libname, api_manager=self)
        pass

    def _unload_library(self, libname: str, *, called_by_worker=False,
                        ____self_destroy____: bool=False):
        if called_by_worker:
            # check libname availability.
            if libname not in self._apihost_info_dataset.keys():
                print("Unloading library: " + libname + "\nLibStat" + str(self._apihost_info_dataset.keys()))
                if self._sysbus.is_running():
                    raise NoSuchLibraryError(libname)
                    # APIManagerError("No such library.", libname)

            request_etype = self._apihost_info_dataset[libname][0]
            request_group = self._apihost_info_dataset[libname][1]
            response_etype = self._apihost_info_dataset[libname][2]
            response_group = self._apihost_info_dataset[libname][3]
            # referenced_count = self._apihost_info_dataset[libname][4]

            # check request object
            if (request_etype, request_group) not in self._group_reference_table:
                raise GroupNotFoundError(request_etype, request_group, "request")
                # APIManagerError("Requested request target not found.", request_etype, request_group)
            # check response object
            if (response_etype, response_group) not in self._group_reference_table:
                raise GroupNotFoundError(response_etype, response_group, "response")
                # APIManagerError("Requested response target not found.", response_etype, response_group)

            if self._group_reference_table[(request_etype, request_group)][1] <= 0:
                if self._group_reference_table[(request_etype, request_group)][1] != 0:
                    print("APIManager: _unload_library: WARNING: Request target reference is less than zero!")
                    self._group_reference_table[(request_etype, request_group)][1] = 0
                    # fix error
                raise APIManagerError("Requested request target is not referenced.")
            if self._group_reference_table[(response_etype, response_group)][0] <= 0:
                if self._group_reference_table[(response_etype, response_group)][0] != 0:
                    print("APIManager: _unload_library: WARNING: Response target reference is less than zero!")
                    self._group_reference_table[(response_etype, response_group)][0] = 0
                    # fix error
                raise APIManagerError("Requested response target is not referenced.")

            # everything has been check.
            self._apihost_info_dataset[libname][4] -= 1  # referenced_count
            self._group_reference_table[(request_etype, request_group)][1] -= 1
            self._group_reference_table[(response_etype, response_group)][0] -= 1

            # in case which self-destroy
            if ____self_destroy____ and not self._sysbus.is_running():
                # check if requested library is fully unloaded.
                if self._apihost_info_dataset[libname][4] <= 0:
                    # now fully unloaded.
                    self._group_reference_table[(request_etype, request_group)][0] -= 1
                    self._group_reference_table[(response_etype, response_group)][1] -= 1
                    pass

                pass
            pass
        else:
            # ignored
            pass

        pass

    def _check_group_add_required(self, listen_etype: str, listen_group: str):
        if (listen_etype, listen_group) not in self._broadcast_refs.keys():
            return True
        return False

    def _check_group_remove_required(self, listen_etype: str, listen_group: str):
        if self._broadcast_refs[(listen_etype, listen_group)][0] == 0 and \
                self._broadcast_refs[(listen_etype, listen_group)][1] == 0:
            return True
        return False
        pass

    def _add_broadcast_receiver_ref(self, listen_etype: str, listen_group: str):
        if self._check_group_add_required(listen_etype, listen_group):
            self._broadcast_refs[(listen_etype, listen_group)] = [0, 0]
        self._broadcast_refs[(listen_etype, listen_group)][0] += 1
        pass

    def _remove_broadcast_receiver_ref(self, listen_etype: str, listen_group: str):
        self._broadcast_refs[(listen_etype, listen_group)][0] -= 1
        if self._check_group_remove_required(listen_etype, listen_group):
            self._broadcast_refs.pop((listen_etype, listen_group))
        pass

    def _add_broadcast_connector_ref(self, listen_etype: str, listen_group: str):
        if self._check_group_add_required(listen_etype, listen_group):
            self._broadcast_refs[(listen_etype, listen_group)] = [0, 0]
        self._broadcast_refs[(listen_etype, listen_group)][1] += 1
        pass

    def _remove_broadcast_connector_ref(self, listen_etype: str, listen_group: str):
        self._broadcast_refs[(listen_etype, listen_group)][1] -= 1
        if self._broadcast_refs[(listen_etype, listen_group)][0] == 0 and \
                self._broadcast_refs[(listen_etype, listen_group)][1] == 0:
            self._broadcast_refs.pop((listen_etype, listen_group))
        pass

    def _create_broadcast_group(self, listen_etype: str, listen_group: str):
        self._sysbus.create_group(listen_etype, listen_group)
        pass

    def _remove_broadcast_group(self, listen_etype: str, listen_group: str):
        self._sysbus.remove_group(listen_etype, listen_group)
        pass

    def create_broadcast_receiver(self, listener_group_etype: str,
                                  listener_group_name: str, cast_name: str):
        create_group = self._check_group_add_required(listener_group_etype,
                                                      listener_group_name)
        if create_group:
            self._create_broadcast_group(listener_group_etype, listener_group_name)

        APIReceiver = _APIBroadcastReceiver(cast_name=cast_name,
                                            listener_group_etype=listener_group_etype,
                                            listener_group_name=listener_group_name,
                                            api_manager=self)
        self._add_broadcast_receiver_ref(listener_group_etype, listener_group_name)
        self._broadcast_receiver_object[cast_name] = APIReceiver
        return APIReceiver
        pass

    def _destroy_broadcast_receiver(self, *, cast_name: str,
                                    listener_group_etype: str,
                                    listener_group_name: str,
                                    called_inside=False):
        if called_inside:
            # we trust all information that provided.
            remove_group = self._check_group_remove_required(listener_group_etype,
                                                             listener_group_name)
            self._broadcast_receiver_object.pop(cast_name)
            if remove_group:
                self._remove_broadcast_group(listener_group_etype, listener_group_name)
            self._remove_broadcast_receiver_ref(listener_group_etype, listener_group_name)
        pass

    def create_brodacast_connecter_by_cast_name(self, sender_name: str, cast_name: str = None):
        if not isinstance(cast_name, str):
            raise TypeError("sender_name should be a str.", cast_name)
        if not isinstance(sender_name, str):
            raise TypeError("sender_name should be a str.", sender_name)
        # query for listener info
        if cast_name not in self._broadcast_receiver_object.keys():
            raise KeyError("requested sender_name is not found.", cast_name)
        # self._lst_group_etype = listener_group_etype
        # self._lst_group_name = listener_group_name
        if not self._broadcast_receiver_object[cast_name]._available:
            raise KeyError("requested sender_name is not found.", cast_name)
        listener_group_name = self._broadcast_receiver_object[cast_name]._lst_group_name
        listener_group_etype = self._broadcast_receiver_object[cast_name]._lst_group_etype
        return self.create_brodacast_connecter(sender_name=sender_name,
                                               listener_group_name=listener_group_name,
                                               listener_group_etype=listener_group_etype)
        pass

    def create_brodacast_connecter(self, sender_name: str, source_module_name: str,
                                   listener_group_etype: str,
                                   listener_group_name: str, *,
                                   allow_invaild_target: bool=True):
        create_group = self._check_group_add_required(listener_group_etype,
                                                      listener_group_name)
        if create_group:
            self._create_broadcast_group(listener_group_etype, listener_group_name)

        APISender = _APIBroadcastConnector(sender_name=sender_name,
                                           source_module_name=source_module_name,
                                           listener_group_etype=listener_group_etype,
                                           listener_group_name=listener_group_name,
                                           api_manager=self,
                                           allow_invaild_target=allow_invaild_target)
        self._add_broadcast_connector_ref(listener_group_etype, listener_group_name)
        self._broadcast_connector_object[sender_name] = APISender
        return APISender
        pass

    def _destroy_broadcast_connector(self, *, sender_name: str,
                                     listener_group_etype: str,
                                     listener_group_name: str,
                                     called_inside=False):
        if called_inside:
            # we trust all information that provided.
            remove_group = self._check_group_remove_required(listener_group_etype,
                                                             listener_group_name)
            self._broadcast_connector_object.pop(sender_name)
            if remove_group:
                self._remove_broadcast_group(listener_group_etype, listener_group_name)
            self._remove_broadcast_connector_ref(listener_group_etype, listener_group_name)
        pass

    pass


class _APIBroadcastReceiver(object):
    def __init__(self, cast_name: str, listener_group_etype: str,
                 listener_group_name: str, *, api_manager: APIManager):
        # check duplicate
        if cast_name in api_manager._broadcast_receiver_object.keys():
            raise DuplicatedBroadcastReceiverError(cast_name)
            # APIManagerError("Duplicated broadcaster name.", cast_name)
        # check types

        if not (isinstance(cast_name, str) or cast_name is None):
            raise TypeError("sender_name should be str.", cast_name)

        # {func_name -> str: {handler: Callable, metadata_kwarg_name: str|None, append_kwargs: dict}}
        self._reg_func_info = dict()

        self._lst_group_etype = listener_group_etype
        self._lst_group_name = listener_group_name
        self._cast_name = cast_name
        self._APIManager = api_manager
        # register executable here.
        self._setup_listener()
        self._available = True
        pass

    def _setup_listener(self):
        self._APIManager._sysbus.add_listener(etype=self._lst_group_etype,
                                              group=self._lst_group_name,
                                              name=self._cast_name,
                                              handler=self._distribute_worker,
                                              append_name="api_context",
                                              metadata_name="metadata")
        pass

    def _takedown_listener(self):
        self._APIManager._sysbus.del_listener(etype=self._lst_group_etype,
                                              group=self._lst_group_name,
                                              name=self._cast_name)

    def _distribute_worker(self, api_context: dict, metadata: dict):
        if self._available:
            # {
            #     "args": {
            #         "args": (arg1, arg2, arg3, ...),
            #         "kwargs": {"something": 1}
            #     }
            # }
            if not metadata["type"] == "APIBroadcastEvent":
                return  # not our business
            # if not api_context["request_target"] == self._cast_name:
            #     return  # do nothing if not ask us to do so.
            for func_name in self._reg_func_info.keys():
                func_context = self._reg_func_info[func_name]
                target_kwargs = dict()
                target_kwargs.update(api_context["args"]["kwargs"])
                # {func_name -> str: {handler: Callable, metadata_kwarg_name: str|None, append_kwargs: dict}}
                if func_context["append_kwargs"] is not None:
                    target_kwargs.update(func_context["append_kwargs"])
                if func_context["metadata_kwarg_name"] is not None:
                    target_kwargs[func_context["metadata_kwarg_name"]] = metadata

                func_context["handler"].__call__(*api_context["args"]["args"],
                                                 **target_kwargs)
        return
        pass

    def register(self, func_name: str, handler: Callable, *, metadata_kwarg_name: str=None,
                 append_kwargs: dict=None):
        pass
        # check types
        if not isinstance(func_name, str):
            raise TypeError("func_name should be a str.", func_name)
        if not callable(handler):
            raise TypeError("handler should be a callable.", handler)
        if not (isinstance(metadata_kwarg_name, str) or metadata_kwarg_name is None):
            raise TypeError("metadata_kwarg_name should be str.", metadata_kwarg_name)
        if not (isinstance(append_kwargs, dict) or append_kwargs is None):
            raise TypeError("append_kwargs should be dict.", append_kwargs)
        self._reg_func_info[func_name] = {
            "handler": handler,
            "metadata_kwarg_name": metadata_kwarg_name,
            "append_kwargs": append_kwargs
        }

    def unregister(self, func_name: str):
        # check types
        if not isinstance(func_name, str):
            raise TypeError("func_name should be a str.", func_name)
        self._reg_func_info.pop(func_name)

    def __del__(self):
        self._available = False
        self._reg_func_info.clear()
        self._takedown_listener()
        self._APIManager._destroy_broadcast_receiver(
            cast_name=self._cast_name,
            listener_group_etype=self._lst_group_etype,
            listener_group_name=self._lst_group_name,
            called_inside=True
        )
    pass


class _APIBroadcastConnector(object):
    # {
    #     "request_target": sender_name
    #     "args": {
    #         "args": (arg1, arg2, arg3, ...),
    #         "kwargs": {"something": 1}
    #     }
    # }
    def __init__(self, sender_name: str, source_module_name: str,
                 listener_group_etype: str,
                 listener_group_name: str, *, allow_invaild_target: bool=True,
                 api_manager: APIManager):
        # check duplicate
        if sender_name in api_manager._broadcast_connector_object.keys():
            raise DuplicatedBroadcastConnectorError(sender_name)
            # APIManagerError("Duplicated broadcast sender name.", sender_name)
        # check types

        if not (isinstance(sender_name, str) or sender_name is None):
            raise TypeError("sender_name should be str.", sender_name)

        # {func_name -> str: {handler: Callable, metadata_kwarg_name: str|None, append_kwargs: dict}}
        self._reg_func_info = dict()

        self._source_module_name = source_module_name
        self._lst_group_etype = listener_group_etype
        self._lst_group_name = listener_group_name
        self._sender_name = sender_name
        self._APIManager = api_manager
        self.allow_invaild_target = allow_invaild_target

        self._available = True

    def call(self, args, kwargs):
        if self._available:
            self._APIManager._sysbus.create_event(etype=self._lst_group_etype,
                                                  source=self._source_module_name,
                                                  target_groups=[self._lst_group_name],
                                                  context={
                                                      "args": {
                                                          "args": args,
                                                          "kwargs": kwargs
                                                      }
                                                  },
                                                  typename="APIBroadcastEvent",
                                                  ignore_not_existed_groups=self.allow_invaild_target)

    def __call__(self, *args, **kwargs):
        self.call(args, kwargs)
        return

    def __del__(self):
        self._available = False
        self._APIManager._destroy_broadcast_connector(sender_name=self._sender_name,
                                                      listener_group_etype=self._lst_group_etype,
                                                      listener_group_name=self._lst_group_name,
                                                      called_inside=True)
    pass


class _APIHost(object):
    def __init__(self, sysbus: EventBus, req_etype: str, req_group: str, resp_etype: str,
                 resp_group: str, *, libname: str=None, api_manager: APIManager):
        self._callers = {}
        # { name: {
        #            "handler": callable,
        #            "metadata_name": metadata_name,
        #            "append_kwargs": append_kwargs
        #         }
        # }
        self.libname = libname
        self._available = True

        # save inputs
        self._req_etype = req_etype
        self._req_group = req_group
        self._resp_etype = resp_etype
        self._resp_group = resp_group
        pass  # need more
        # API Host should create listen group for getting requests.
        # In case of name duplicate, we detect that. need API Manager.
        #
        self._APIManager = api_manager

        self._sysbus = sysbus

        sysbus.add_listener(req_etype, req_group, "Library: " + self.libname,
                            self._req_worker, metadata_name="metadata")
        pass

    def _req_worker(self, context, metadata):
        if not self._available:
            return
        # context structs:
        # {
        #     "libname": libname,
        #     "function": function_name,
        #     "metadata": { --- metadata ---- } # from bus
        #     "args": {
        #         "args": (arg1, arg2, arg3, ...),
        #         "kwargs": {"something": 1}
        #     }
        #     "lock_info": (token_name, Lock())
        # }
        # metadata name query from self._callers

        try:
            if not metadata["type"] == "APIRequestEvent":
                # except rule: if someone is querying for function list
                if metadata["type"] == "APIQueryFunctionListEvent":
                    query_list = self.get_function_list()
                    #print("before_event")
                    self._sysbus.create_event(etype=self._resp_etype,
                                              source="Library: " + self.libname,
                                              target_groups=[self._resp_group],
                                              context={
                                                  "client_token": context["client_token"],
                                                  "functions": query_list,
                                                  "lock_info": context["lock_info"]
                                              }, typename="APIFunctionListUpdate")
                    # print("after_event")
                    return  # finish job
                return  # not a API request. do nothing

            if not context["libname"] == self.libname:
                return  # do nothing

            function_name = context["function"]

            if function_name not in self._callers.keys():
                # return exception class
                self._sysbus.create_event(etype=self._resp_etype,
                                          source="Library: " + self.libname,
                                          target_groups=[self._resp_group],
                                          context={
                                            "client_token": context["client_token"],
                                            "exception": NoSuchFunctionError(function_name, self.libname),
                                            "lock_info": context["lock_info"]
                                            }, typename="APIExceptionEvent")
                pass
        except KeyError:
            # seems this is not sent by APIClient. Do nothing.
            return
            pass

        push_args = context["args"]["args"]
        push_kwargs = context["args"]["kwargs"]

        if self._callers[function_name]["metadata_name"] is not None:
            push_kwargs[self._callers[function_name]["metadata_name"]] = metadata

        if self._callers[function_name]["append_kwargs"] is not None:
            push_kwargs.append(self._callers[function_name]["append_kwargs"])

        try:
            return_val = self._callers[function_name]["handler"].__call__(*push_args, **push_kwargs)
        except Exception as e:
            # we caught some exception here.
            self._sysbus.create_event(etype=self._resp_etype, source="Library: " + self.libname,
                                      target_groups=[self._resp_group], context={
                                            "client_token": context["client_token"],
                                            "exception": e,
                                            "lock_info": context["lock_info"]
                                        }, typename="APIExceptionEvent")
            return
            pass

        # we got return values.
        self._sysbus.create_event(etype=self._resp_etype, source="Library: "+self.libname,
                                  target_groups=[self._resp_group], context={
                                    "client_token": context["client_token"],
                                    "return": return_val,
                                    "lock_info": context["lock_info"]
                                  }, typename="APIResponseEvent")
        return
        pass

    def register(self, function_name: str, handler: Callable, *, metadata_kwarg_name: str = None,
                 append_kwargs: dict=None):
        if not self._available:
            raise NotAvailableError(self, self.libname)
            # APIManagerError("This _APIHost is no longer available.", self.libname)
        # check duplicate
        if function_name in self._callers.keys():
            raise DuplicatedFunctionError(function_name, self.libname)
            # APIManagerError("Duplicated function name.", self.libname, function_name)
        # check types
        if not callable(handler):
            raise TypeError("handler should be a callable.", handler)

        if not (isinstance(function_name, str) or function_name is None):
            raise TypeError("function_name should be str.", function_name)

        if not (isinstance(metadata_kwarg_name, str) or metadata_kwarg_name is None):
            raise TypeError("metadata_kwarg_name should be str.", metadata_kwarg_name)

        if not (isinstance(append_kwargs, dict) or append_kwargs is None):
            raise TypeError("append_kwargs should be dict.", append_kwargs)

        # nothing going wrong, do register.
        self._callers[function_name] = {
            "handler": handler,
            "metadata_name": metadata_kwarg_name,
            "append_kwargs": append_kwargs
        }

        # Send notify event to let all clients update their function list.
        if self._sysbus.is_running():
            self._sysbus.create_event(etype=self._req_etype, source=self.libname,
                                      target_groups=[self._req_group], context={},
                                      typename="APIQueryFunctionList")
        pass

    def unregister(self, function_name: str):
        if not self._available:
            raise NotAvailableError(self, self.libname)
            # APIManagerError("This _APIHost is no longer available.", self.libname)
        if function_name not in self._callers.keys():
            raise NoSuchFunctionError(function_name, self.libname)
            # APIManagerError("No such funcion.", self.libname, function_name)
        self._callers.pop(function_name)
        # Send notify event to let all clients update their function list.
        if self._sysbus.is_running():
            self._sysbus.create_event(etype=self._req_etype, source=self.libname,
                                      target_groups=[self._req_group], context={},
                                      typename="APIQueryFunctionList")
        pass

    def _unregister_all(self):
        if not self._available:
            raise NotAvailableError(self, self.libname)
            # raise APIManagerError("This _APIHost is no longer available.", self.libname)
        # just do some cleanup.
        for function_name in list(self._callers.keys()):
            self.unregister(function_name=function_name)
        pass

    def get_function_list(self):
        return list(self._callers.keys())

    def ____self_destroy____(self, *, __confirm__=False):
        if __confirm__:
            self._available = False
            self._APIManager._destroy_library(self.libname, called_by_worker=True,
                                              ____self_destroy____=True)
            pass

    def __del__(self):
        if not self._sysbus.is_running():
            self.____self_destroy____(__confirm__=True)
            return
        print("Requesting destory library: " + self.libname)
        if self._available:
            self._unregister_all()
            self._available = False
            self._APIManager._destroy_library(self.libname, called_by_worker=True)
        else:
            print("_APIHost: WARNING: Attempt to double-free.")
        pass

    def __repr__(self):
        if self._available:
            return "<_APIHost object: '" + self.libname + "' at " + str(hex(id(self)))+">"
        else:
            return "<[UNAVAILABLE] _APIHost object: '" + self.libname + "' at " + str(hex(id(self))) + ">"
    pass


class _APIClient(object):
    def __init__(self, sysbus: EventBus, req_etype: str, req_group: str, resp_etype: str,
                 resp_group: str, client_name: str, *, libname: str, api_manager: APIManager):
        self._token = client_name + " " + libname + " " + str(randint(0, 0xFFFF))

        self._lock_container = dict()
        self.libname = libname
        self._req_etype = req_etype
        self._req_group = req_group
        self._resp_etype = resp_etype
        self._resp_group = resp_group
        self._client_name = client_name
        self._APIManager = api_manager
        # setup listeners
        self._sysbus = sysbus

        self._sysbus.add_listener(etype=resp_etype, group=resp_group,
                                  name=libname + "/" + client_name, handler=self._resp_worker,
                                  metadata_name="metadata")

        self._function_list = []
        self._available = True

        if sysbus.is_running():
            self._get_function_list_refresh(aasync=False)
        else:
            self._get_function_list_refresh(aasync=True)

        pass

    def _create_lock_object(self):
        # generate a new token:

        # got token and being occupied
        # create new lock
        lock = Lock()
        # occupy the lock
        lock.acquire()

        while True:
            lock_token = randint(0x00000000, 0xFFFFFFFF)
            if lock_token not in self._lock_container.keys():
                self._lock_container[lock_token] = ("lock", lock)
                break

        return lock_token, lock

    def call_api(self, function_name, args, kwargs):
        # context structs:
        # {
        #     "libname": libname,
        #     "function": function_name,
        #     "args": {
        #         "args": (arg1, arg2, arg3, ...),
        #         "kwargs": {"something": 1}
        #     }
        #     "lock_info": (token_name, Lock())
        # }

        if not self._available:
            raise NotAvailableError(self, self.libname)
            # APIManagerError("This _APIClient is no longer available.", self.libname)

        lock_obj = self._create_lock_object()

        self._sysbus.create_event(etype=self._req_etype, source=self._client_name,
                                  target_groups=[self._req_group], context={
                                    "libname": self.libname,
                                    "client_token": self._token,
                                    "function": function_name,
                                    "args": {
                                        "args": args,
                                        "kwargs": kwargs
                                    },
                                    "lock_info": lock_obj
                                  }, typename="APIRequestEvent")

        # event sent, now acquire lock more times.
        if not self._available:
            raise NotAvailableError(self, self.libname)
            # APIManagerError("This _APIClient is no longer available.", self.libname)
        lock_obj[1].acquire()
        # released.
        lock_obj[1].release()
        # get value and remove from list.
        status, value = self._lock_container.pop(lock_obj[0])
        if status == "return":
            return value
        elif status == "exception":
            raise value  # the original exception will re-raise here.
        else:
            assert "_APIClient: Unexpected retuen value in self._lock_container"
        pass

    def _resp_worker(self, context: dict, metadata: dict):
        # TODO: MORE DEBUG REQUIRED
        #import sys, threading
        # print("Current: " + str(sys._getframe().f_lineno) + " @ " + sys._getframe().f_code.co_filename)
        # print("Last   : " + str(sys._getframe().f_back.f_lineno) + " @ " + sys._getframe().f_back.f_code.co_filename)
        # print("Current thread: " + threading.current_thread().name)
        # print("test")
        # received return value or exception or update list.
        # check that data were sent by us.
        if not context["client_token"] == self._token:
            # TODO: MORE DEBUG REQUIRED
            #print("DEBUG: Skipped  : " + str(context["client_token"]))
            #print("DEBUG: Expecting: " + self._token + "\n")
            return
        # check metadata to confirm event type.
        if metadata["type"] == "APIFunctionListUpdate":
            self._function_list = context["functions"]
            if "lock_info" in context.keys():
                # if there are lock, we release it.
                if context["lock_info"][1].locked():
                    # print(context["lock_info"][0].__repr__() + " " + context["lock_info"][1].__repr__())
                    context["lock_info"][1].release()
                self._lock_container.pop(context["lock_info"][0])
        elif metadata["type"] == "APIResponseEvent":
            self._lock_container[context["lock_info"][0]] = ("return", context["return"])
            # print(context["lock_info"][0].__repr__() + " " + context["lock_info"][1].__repr__())
            context["lock_info"][1].release()
        elif metadata["type"] == "APIExceptionEvent":
            self._lock_container[context["lock_info"][0]] = ("exception", context["exception"])
            context["lock_info"][1].release()
        else:
            return  # not our business
        pass

    def get_function_list(self, update=False):
        if update:
            self._get_function_list_refresh()
        return self._function_list
        pass

    def _get_function_list_refresh(self,aasync=False):
        aasync = False
        context = {}
        locker = tuple()
        if aasync:
            # send event only
            pass
        else:
            # send event and wait for return
            locker = self._create_lock_object()
            context.update({"lock_info": locker,
                            "client_token": self._token})
            pass
        self._sysbus.create_event(etype=self._req_etype, source=self._client_name,
                                  target_groups=[self._req_group], context=context,
                                  typename="APIQueryFunctionListEvent")
        if not aasync:
            locker[1].acquire()
            locker[1].release()
            if not self._available:
                raise NotAvailableError(self, self.libname)
                # APIManagerError("This _APIClient is no longer available.", self.libname)
        pass

    def check_function_availablity(self, function_name):
        return function_name in self._function_list
        pass

    def spawn_caller_module(self, active_function_sync: bool=False, *, ignore_api_check: bool=False):
        # spawn new module
        return APICallerModule(self,
                               always_update_when_check=bool(active_function_sync),
                               ignore_api_check=bool(ignore_api_check)
                               )

    def ____self_destroy____(self, *, __confirm__=False):
        if __confirm__:
            self._available = False
            # release all lock
            for lock_token in list(self._lock_container.keys()):
                flag, lock = self._lock_container[lock_token]
                if flag == "lock":
                    lock = self._lock_container[lock_token]["lock"]
                    self._lock_container[lock_token] = \
                        ("exception",
                            NotAvailableError(self, self.libname)
                         )
                    lock.release()

            pass

    def __del__(self):
        if not self._sysbus.is_running():
            self.____self_destroy____(__confirm__=True)
            return
        print("Requesting unload library: " + self.libname)
        self._APIManager._unload_library(self.libname, called_by_worker=True)
        pass

    def __repr__(self):
        return "<_APIClient object: '" + self.libname + "' at " + str(hex(id(self)))+">"
    pass


class APICallerModule(object):
    def __init__(self, client_obj: _APIClient, *,
                 call_target: str=None, ignore_api_check: bool=False,
                 always_update_when_check: bool=False):
        # just use the reference to call something.
        self._client_obj = client_obj
        self._call_target = call_target
        self._ignore_api_check = ignore_api_check
        self.always_update_when_check = always_update_when_check
        self._first_start = True

        pass

    def caller_module_type(self):
        if self._call_target is not None:
            return "function"
        else:
            return "library"

    def __getattr__(self, item):
        # print(self._call_target)
        if self._call_target is not None:
            if item == "__call__":
                return self
            else:
                return super().__getattribute__(item)
        # print("before APICheck stat")
        if self._ignore_api_check:
            return APICallerModule(self._client_obj, call_target=item,
                                   ignore_api_check=True)

        # query
        # print("before 1st start")
        if self._first_start:
            # print("before update")
            self._client_obj.get_function_list(update=True)

        # check availability
        # print("before check availablity")
        if item in self._client_obj.get_function_list(
                update=self.always_update_when_check):
            return APICallerModule(self._client_obj, call_target=item,
                                   ignore_api_check=False)

        # finalize
        return super().__getattribute__(item)
        pass

    def __call__(self, *args, **kwargs):
        if self._call_target is not None:
            return self._client_obj.call_api(self._call_target, args, kwargs)
        else:
            raise TypeError("'APICallerModule Library' object is not callable")
        pass

    # def __dir__(self):
    #     container = ["caller_module_type"]
    #     container.extend(super().__dir__())
    #
    #     if self._call_target is not None:
    #         container.append("__call__")
    #         return container
    #
    #     if not self._ignore_api_check:
    #         container.extend(
    #             self._client_obj.get_function_list(self.always_update_when_check)
    #         )
    #     return container

    def __repr__(self):
        if self._call_target is not None:
            return "<APICallerModule Function '" + \
                self._call_target + "' of Library '" + \
                   self._client_obj.libname + "' at " + str(hex(id(self))) + ">"
        else:
            # library object
            return "<APICallerModule Library '" + \
                   self._client_obj.libname + "' at " + str(hex(id(self))) + ">"
            pass
        pass


class APILoader(object):
    def __init__(self, api_manager: APIManager, sysbus: EventBus):
        self._api_manager = api_manager
        self._sysbus = sysbus

        # global operation lock
        self._loader_lock = Lock()

        self._available_module_table = dict()  # store bare module object
        self._active_module_table = dict()  # store bare module object
        self._module_dep_table = dict()  # store module depend info
        self._module_ref_table = dict()  # store module reference info
        self._module_direct_ref_count = dict()  # store module direct reference count

        self._system_operation_lock = Lock()
        self._system_operation_lock.acquire()
        self._refresh_module_table()
        self._system_operation_lock.release()

    def ____acquire_control____(self):
        self._loader_lock.acquire()
        self._loader_lock.release()
        self._loader_lock.acquire()

    def ____release_control____(self):
        self._loader_lock.release()

    def ____wait_system_lock____(self):
        self._system_operation_lock.acquire()
        self._system_operation_lock.release()

    def _refresh_module_table(self, *, ignore_error=False):
        self.____acquire_control____()
        try:
            self._available_module_table.update(Probe())
            # prepare dependence table
            deptable = dict()
            for mod_name in self._available_module_table:
                modinfo = self._available_module_table[mod_name].ModuleInfo()
                if not modinfo["name"] == mod_name:
                    if ignore_error:
                        print("Error loading module: " + mod_name + ": Module format error.")
                        continue
                    else:
                        raise ModuleFormatError(modinfo["name"], mod_name)
                deptable[mod_name] = modinfo
            # deptable is ready
            self._module_dep_table.update(deptable)
        finally:
            self.____release_control____()

    def _system_startup(self):
        self._system_operation_lock.acquire()
        try:
            # read config
            cfg = ConfigManager.load_config("SystemStartup")
            enabled_adaptors = cfg["enabled-adaptors"]
            # debug
            # print("_system_startup: enabled adaptors: " + str(enabled_adaptors))
            for adaptor in enabled_adaptors:
                try:
                    self._load_module(adaptor)
                except ModuleDependencyError:
                    print("WARNING: Failed loading adaptor '" + adaptor + "' during startup process.")
                    from traceback import print_exc
                    print_exc()
        finally:
            self._system_operation_lock.release()
        pass

    def _system_shutdown(self):
        self._system_operation_lock.acquire()
        try:
            # to shutdown the system, just dep-unload LiveComFW dummy module.
            self._unload_module("LiveComFW")
            pass
        finally:
            self._system_operation_lock.release()

    def _dep_load_probe(self, module_name, module_version=None):
        # version = None means any.
        deptable = []
        if module_name in self._module_dep_table.keys():
            # we have this module. Check version.
            if module_version is not None:
                if module_version > self._module_dep_table[module_name]["version"]:
                    raise ModuleDependencyError(
                        "version", (module_name, module_version,
                                    self._module_dep_table[module_name]["version"]))
                if "deprecated" in self._module_dep_table[module_name].keys() and \
                        module_version < self._module_dep_table[module_name]["deprecated"]:
                    raise ModuleDependencyError(
                        "deprecated", (module_name, module_version,
                                       self._module_dep_table[module_name]["deprecated"]))
            deptable.append(module_name)
            for dep, ver in self._module_dep_table[module_name]["require"]:
                deptable.extend(self._dep_load_probe(dep, ver))
        else:
            # module not found.
            raise ModuleDependencyError("no_available", (module_name, module_version))
            pass
        return deptable
        pass

    def _check_load_dep(self, mod_name: str):
        load_seq = []
        for mod_name in self._dep_load_probe(mod_name)[::-1]:
            if mod_name in load_seq:
                continue
            elif mod_name in self._active_module_table.keys():
                continue

            load_seq.append(mod_name)

        # debug
        # print("_check_load_dep: sequence: " + str(load_seq))
        return load_seq
        pass

    def _check_unload_dep(self, module_name):
        load_seq = []
        for mod_name in self._dep_unload_probe(module_name)[::-1]:
            if mod_name in load_seq:
                continue
            # elif mod_name in self._active_module_table.keys():
            #     continue

            load_seq.append(mod_name)

        # debug
        # print("_check_unload_dep: sequence: " + str(load_seq))
        return load_seq

    def _dep_unload_probe(self, module_name: str):
        # _module_dep_table[mod_name] = {
        #     "name": "Services.MessageRouter",
        #     "Version": "0.1",
        #     "require": [
        #         ("LiveComFW", "0.1")
        #     ]
        # }

        # use reference table and reference count to unload module.
        # we only probe the unload sequence here.
        deptable = [module_name]
        if module_name not in self._active_module_table.keys():
            # something went wrong.
            assert "Unloading module that not existed: " + module_name
        for depmod in self._module_ref_table[module_name]:
            deptable.extend(self._dep_unload_probe(depmod))
        return deptable
        pass

    def load_module(self, mod_name: str):
        # check system lock
        self.____wait_system_lock____()
        self._load_module(mod_name)
        pass

    def _load_module(self, mod_name: str):
        self.____acquire_control____()
        # debug
        # print("_load_module: loading target module: " + mod_name)
        try:
            for load_mod in self._check_load_dep(mod_name):
                if load_mod in self._module_dep_table.keys():
                    if "description" in self._module_dep_table[load_mod].keys():
                        print("LiveCom: 模块管理: 正在启用模块: " +
                              self._module_dep_table[load_mod]["description"] +
                              " (" + load_mod + ")")
                    else:
                        print("LiveCom: 模块管理: 正在启用模块: " + load_mod)
                # debug
                # print("_load_module: setting up module: " + load_mod)
                self._mod_setup(load_mod)
        finally:
            self.____release_control____()

    def _mod_setup(self, mod_name: str):
        # debug
        # print("_mod_setup: setting up module: " + mod_name)
        # execute setup
        self._available_module_table[mod_name].Setup(self._sysbus)
        # if everything is ok, copy reference to active module table.
        self._active_module_table[mod_name] = self._available_module_table[mod_name]
        # register reference.
        self._module_ref_table[mod_name] = []
        for mod_dep_setup_name, mod_dep_ver in self._module_dep_table[mod_name]["require"]:
            self._module_ref_table[mod_dep_setup_name].append(mod_name)
            self._module_direct_ref_count[mod_dep_setup_name] = 1 if \
                mod_dep_setup_name not in self._module_direct_ref_count else \
                (self._module_direct_ref_count[mod_dep_setup_name] + 1)
            # debug
            # print("_mod_setup: setting reference for "+ mod_dep_setup_name + ", reference count is " + \
            #  str(self._module_direct_ref_count[mod_dep_setup_name]))
        # this module was finished setup.
        pass

    def unload_module(self, mod_name: str):
        self.____wait_system_lock____()
        self._unload_module(mod_name)

    def _unload_module(self, mod_name: str):
        self.____acquire_control____()
        # debug
        # print("_unload_module: unloading target module: " + mod_name)
        try:
            for module_name in self._check_unload_dep(mod_name):
                # debug
                # print("_unload_module: taking down:" + module_name)
                self._mod_takedown(module_name)
        finally:
            self.____release_control____()
        pass

    def _mod_takedown(self, mod_name: str):
        # debug
        # print("_mod_takedown: unloading module: " + mod_name + "; remaining_ref_count=" +
        #       str(0) if mod_name not in self._module_direct_ref_count else \
        #     str(self._module_direct_ref_count[mod_name]))

        if mod_name not in self._module_direct_ref_count or \
                self._module_direct_ref_count[mod_name] <= 0:
            # execute takedown
            if mod_name in self._module_dep_table.keys():
                if "description" in self._module_dep_table[mod_name].keys():
                    print("LiveCom: 模块管理: 正在停用模块: " +
                          self._module_dep_table[mod_name]["description"] +
                          " (" + mod_name + ")")
                else:
                    print("LiveCom: 模块管理: 正在停用模块: " + mod_name)
            self._active_module_table[mod_name].Takedown(self._sysbus)
            # deref everything
            for module_name in self._module_ref_table[mod_name]:
                # if there throw KeyError, that means something went very wrong.
                self._module_direct_ref_count[module_name] -= 1
            self._module_ref_table.pop(mod_name)
            self._active_module_table.pop(mod_name)
            # debug
            # print("_mod_takedown: unloaded: " + mod_name)
        # if reference count is not zero, ignore.
        pass
