from traceback import print_exc

import LiveCom.SystemComponents.EventBus as msgbus
import time

bus = msgbus.EventBus(name="TestBus")
status = False

def testHandler(context):
    print("testHandler: " + str(context))
    status = True

if bus.create_group(bus.EType.INCOMING, "TestReceiveGroup"):
    print("Create group success")
    print("GroupList: " + str(bus.get_all_groups()))
else:
    assert "Create group failed."

if bus.add_listener(bus.EType.INCOMING, "TestReceiveGroup", "TestReceiverName", testHandler):
    print("Create Listener success")
    print("ListenerList: " + str(bus.get_listeners_from_group(bus.EType.INCOMING, "TestReceiveGroup")))
else:
    assert "Create Listener failed."

bus.start_loop()

bus.create_event(bus.EType.INCOMING, "testSource", ["TestReceiveGroup"], {"testval": "test!!"})

time.sleep(3)

if status==False:
    assert "No event triggered."

status = False

def testHandlerEX(argins, othername, *, kwargins, metadata):
    print("ArgIns: " + str(argins))
    print("KwArgIns: " + str(kwargins))
    print("OtherName:" + str(othername))
    print("metadata: " + str(metadata))
    status=True

# dynamic add listener
if bus.add_listener(bus.EType.INCOMING, "TestReceiveGroup", "TestOtherName", testHandlerEX, args=("ArgInsSuccess", ), kwargs={"kwargins": "KwArgInsSuccess"}, append_name="othername", metadata_name="metadata"):
    print("Create Online add Listener success")
    print("ListenerList: " + str(bus.get_listeners_from_group(bus.EType.INCOMING, "TestReceiveGroup")))
else:
    assert "Create Online add Listener failed."

bus.create_event(bus.EType.INCOMING, "testSource", ["TestReceiveGroup"], {"testval": "test!!"})

time.sleep(3)

if status==False:
    assert "No extra event triggered."

status = False

print("Exception handling test: ========== Showing traceback is NORMAL here! =============")

try:
    bus.get_listeners_from_group(bus.EType.INCOMING, "dasdasdasdasdas")
except msgbus.NoSuchGroupError as e:
    if e.etype == bus.EType.INCOMING and e.group_name == "dasdasdasdasdas":
        print_exc()
        status = True
finally:
    print("End of exception handling test.")

if status==False:
    assert "Exception handling error!"

bus.stop_loop()
print("finished")
print("GC collected refs: "+ str(bus._gc_collected_refs))

