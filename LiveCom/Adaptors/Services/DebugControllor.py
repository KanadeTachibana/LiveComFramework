
def ModuleInfo():
    return {
        "name": "Services.DebugControllor",
        "description": "调试控制器",
        "version": "1.0",
        "deprecated": "0.0",
        "require": [
            ("Services.MessageRouter", "0.1"),
            ("Services.SessionManager", "0.1"),
            ("Backend.NPSSI", "0.1")
            # nothing required.
        ]
    }

from LiveCom.SystemComponents.EventBus import EventBus  # load templates
from LiveCom.SystemComponents.API_Demo import APIManager, _APIClient
import LiveCom.Auxiliaries.LiveCom.SessionManager.SessionUtilsLibTemplates as APIClient_SessionMgr

sysbus = EventBus()
APIClient_Router = _APIClient(sysbus, "", "", "", "", "", libname="", api_manager=APIManager(sysbus))


# "CQHTTP:G187233857": ["CQHTTP:G688097151", "CQHTTP:G720836502"],
# "CQHTTP:G688097151": ["CQHTTP:G187233857"],
# "CQHTTP:G720836502": ["CQHTTP:G187233857"]

def Setup(sysbus: EventBus):
    global APIClient_Router
    global APIClient_SessionMgr
    global callers
    APIClient_Router = sysbus.APIManager.load_library("MessageRouterControl")
    # APIHost.register("add_routing_group", add_routing_group)
    # APIHost.register("del_routing_group", del_routing_group)
    # APIHost.register("set_routing_link", set_routing_link)
    # APIHost.register("unset_routing_link", unset_routing_link)
    # APIHost.register("get_groups", get_groups)
    # APIHost.register("get_group_context", get_group_context)
    print("DEBUGCTL: Setting up routes...")
    callers = APIClient_Router.spawn_caller_module()

    callers.add_routing_group("Debug")
    callers.set_routing_link("Debug", "1stDim->All", "CQHTTP:G187233857",
                             ["CQHTTP:G688097151", "CQHTTP:G720836502"])
    callers.set_routing_link("Debug", "TestSpace->1stDim", "CQHTTP:G688097151",
                             ["CQHTTP:G187233857"])
    callers.set_routing_link("Debug", "2ndDim->1stDim", "CQHTTP:G720836502",
                             ["CQHTTP:G187233857"])
    print("DEBUGCTL: Route setup done.")

    APIClient_SessionMgr = sysbus.APIManager.load_library("SessionUtilsLib")
    sessmgr = APIClient_SessionMgr.spawn_caller_module()
    print("DEBUGCTL: Starting test session.")
    # sessmgr.CreateSession("CQHTTP:@F:U1491124389")

    pass

def Takedown(sysbus):
    # do nothing at all
    callers.del_routing_group("Debug")
    pass