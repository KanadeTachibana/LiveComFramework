from LiveCom.SystemComponents.EventBus import EventBus
from LiveCom.SystemComponents.API_Demo import _APIBroadcastReceiver, APIManager
import json

incoming_receiver = _APIBroadcastReceiver("","","", api_manager=APIManager(EventBus()))
outgoing_receiver = _APIBroadcastReceiver("","","", api_manager=APIManager(EventBus()))

from threading import Lock
msg_op_lock = Lock()

debug_enabled=False

def ModuleInfo():
    return {
        "name": "Services.DebugDisplay",
        "version": "0.1",
        "deprecated": "0.0",
        "require": [
            ("LiveComFW", "0.1")
        ]
    }

from pprint import pprint

def show_message(context, metadata, src_type):
    if debug_enabled:
        # print(
        #     "SrcType:  " + str(src_type) + "\n" +
        #     "\tContext:  " + str(context) + "\n" +
        #     "\tMetadata: " + str(metadata) + "\n"
        # )
        msg_op_lock.acquire()
        #pprint(
        print(json.dumps({
            "SrcType": src_type,
            "context": context,
            "metadata": metadata
        }, indent=4))
        print("")
        msg_op_lock.release()
        pass
#
# def showdict(d: dict, prefix=""):
#     for kn in d.keys():
#         print(prefix + '"' + kn + '": ', end='')
#         pprint(d[kn], indent=len(prefix)+len(kn)+5)

def Takedown(sysbus: EventBus):
    incoming_receiver.unregister("Receiver")
    outgoing_receiver.unregister("Receiver")
    pass

def Setup(sysbus: EventBus):
    global incoming_receiver
    global outgoing_receiver
    incoming_receiver = sysbus.APIManager.create_broadcast_receiver(EventBus.EType.INCOMING,
                                                                    "UnifiedMessageProcessors", "DebugDisplay-Incoming")
    outgoing_receiver = sysbus.APIManager.create_broadcast_receiver(EventBus.EType.OUTGOING,
                                                                    "UnifiedMessageProcessors", "DebugDisplay-Outgoing")
    incoming_receiver.register("Receiver", show_message,
                               metadata_kwarg_name="metadata",
                               append_kwargs={"src_type": "IncomingBus"})
    outgoing_receiver.register("Receiver", show_message,
                               metadata_kwarg_name="metadata",
                               append_kwargs={"src_type": "OutgoingBus"})
    pass

