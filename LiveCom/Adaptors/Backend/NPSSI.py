from LiveCom.Auxiliaries.LiveCom.NPSSI.NPSSIExceptions import *
from LiveCom.Auxiliaries.LiveCom.SessionManager.SessionManagerExceptions import *
from LiveCom.Auxiliaries.LiveCom.NPSSI.ObjectBuffers import StreamObjectBuffer
from LiveCom.Auxiliaries.LiveCom.NPSSI.ConsoleIO_Utils import ConsoleIO
from LiveCom.Auxiliaries.LiveCom.MessageTargetUtils import conv_container
from collections import defaultdict
import LiveCom.Auxiliaries.LiveCom.SessionManager.SMBLibTemplates as _smblib
from threading import Lock
from LiveCom.Auxiliaries.LiveCom.API.APIToolkits import APIUMPModuleUtils
from LiveCom.Config.LoadConfig import load_config
from LiveCom.SystemComponents.EventBus import EventBus
from LiveCom.Auxiliaries.LiveCom.NPSSI.TerminalAllocate import *

import os

APIClient_SMBLib = None
SMBLib = _smblib

NPSSI = "NPSSI"
MODULE_NAME = "Backend.NPSSI"

APIHost = None

applications = dict()
# "LocalShell": {
#     "description": "默认的系统Shell",
#     "executable": "cmd.exe",
#     "args": [],
#     "endl": "\r\n",
#     "encoding": "gbk"

task_struct = dict()
# {
#     TID: {
#         "SessionID": 1,
#         "Process": ConsoleIO()
#     }
# }



class Process(object):
    def __init__(self, terminal_id: str, cmdargs: list, app_name: str="DefaultNPSSIApp",
                 encoding: str="UTF-8", eol: str="\n"):
        self._consoleio = ConsoleIO(cmdargs, self._stdout_handler, None, app_name, False,
                                    os.getcwd(), True, None, {"handler": self._stop_handler,
                                                              "args": tuple(), "kwargs": {}}, encoding, eol)
        self._return_lock = Lock()  # we will never release this lock.
        self._stream_cache = StreamObjectBuffer(self._output_handler, name=terminal_id + " " + app_name)
        self.terminal_id = terminal_id
        self.app_name = app_name
        self.cmdargs = cmdargs
        self.encoding = encoding
        self.eol = eol
        self._op_lock = Lock()

    def start_task(self):
        if not self._consoleio.started:
            self._return_lock.acquire()
            self._consoleio.start()
        return self._return_lock
        pass

    def writeline(self, data: str):
        self._consoleio.writeline(data)
        pass

    def stop_task(self, force: bool=False):
        if force:
            self._consoleio.kill()
        else:
            self._consoleio.terminate()

    def _stdout_handler(self, data: str):
        # 送到缓冲区里面
        self._stream_cache.add_content(data)
        pass

    def _output_handler(self, datalist: list):
        self._op_lock.acquire()
        txt = ""
        for line in datalist:
            txt += line.rstrip(self.eol) + self.eol
        txt = txt.rstrip(self.eol)
        send_data(self.terminal_id, txt)
        self._op_lock.release()

    def _stop_handler(self):
        self._stream_cache.wait_timer_task_done()
        self._op_lock.acquire()
        self._op_lock.release()
        retval = self._consoleio.returncode
        exit_event(self.terminal_id, retval)

        pass

    def is_running(self):
        return self._consoleio.is_running()

# 内部用途接口

sysbus = EventBus()
UMPLib = None # APIUMPModuleUtils(sysbus, "Backend.NPSSI", NPSSI)
broadcast_receiver = None # UMPLib.receiver
broadcast_sender = None # UMPLib.connector


def send_data(terminal_id: str, data: str):
    #print("send data: " + data + " terminal=" + terminal_id)
    broadcast_sender({
        "adaptor": MODULE_NAME,  # 适配器名称（也就是注册模块时的名称）
        "source": terminal_id,  # 标记来源
        "message": data,  # 消息文本，使用内联消息格式。
        "platform_context": {
            "session_id": task_struct[terminal_id]["SessionID"]
        },
        "routing_path": []  # 源输入适配器此处可以留空。
    })
    pass

def exit_event(terminal_id, return_val: int):
    #print("exit_event: " + terminal_id)
    SMBLib.AppExitEvent(
        task_struct[terminal_id]["SessionID"],
        terminal_id,
        return_val
    )
    task_struct.pop(terminal_id)
    pass

def data_handler(context: dict):
    # outgoing_context = {
    #     "target": "INTERFACE:CONTAINER:USER",  # 标记目标
    #     "message": "inline[type=\"format\", context=\"1\"]of text",  # 消息文本，使用内联消息格式。
    #     "routing_path": [  # 消息路由器负责将前一个消息的信息加入这里。[0]始终是当前信息。[1]是前一跳的信息。[-1]是初始消息。
    #         {
    #             "adaptor": "ADAPTOR.NAME",
    #             "source": "INTERFACE:CONTAINER:USER",
    #             "message" "data data data..."
    #             "target": "INTERFACE:CONTAINER:USER",
    #             "platform_context": {
    #                 # 平台相关上下文信息
    #             }
    #         }
    #     ]
    # }
    target = conv_container(context["target"])
    if target in task_struct.keys():
        task_struct[target]["Process"].writeline(context["message"])
    pass





# {
#     "LocalShell": {
#         "description": "默认的系统Shell",
#         "executable": "cmd.exe",
#         "args": [],
#         "endl": "\r\n",
#         "encoding": "gbk"
#       }
# }


# 由此开始设计NPSSI的API回调接口。
# def CheckApp(app_name: str):  # 检查apterminal_id是否可用
#     pass

# task_struct = {
#     "stage=terminal": {
#         "stage=process": NProcessObject
#     }
# }


# 加载app但是不立即启动
def OpenApp(session_id: int, app_name: str, arg_info: None) -> str:
    if arg_info is None:
        arg_info = {
            "append_argv": []
        }
    # 检查是否是自己的任务
    backend_name = app_name.split("/", maxsplit=1)[0]
    if not backend_name == NPSSI:
        raise NotOurBusinessError(app_name)
    local_app_name = app_name.split("/", maxsplit=1)[1]
    if local_app_name not in applications.keys():
        raise NoSuchAppError(app_name)

    argvs = applications[local_app_name]["args"]
    executable = applications[local_app_name]["executable"]
    encoding = applications[local_app_name]["encoding"]
    endl = applications[local_app_name]["endl"]
    cmdargs = []
    cmdargs.extend(arg_info["append_argv"])
    cmdargs.append(executable.strip('"'))
    cmdargs.extend(argvs)

    terminal_id = allocate_next_terminal(task_struct)

    task_struct[terminal_id] = {
        "Process": Process(terminal_id, cmdargs, app_name, encoding, endl),
        "SessionID": session_id
    }
    print("NPSSI: 启动应用: " + app_name + " 位于终端" + terminal_id)
    return terminal_id
    pass

def CloseApp(session_id: int, terminal_id: str, force: bool=False):  # 关闭app，可以指定关闭方式（terminate和kill，虽然在windows上没啥区别。）
    if terminal_id not in task_struct.keys():
        raise NoSuchTerminalError(terminal_id)

    tstruct = task_struct[terminal_id]
    if tstruct["SessionID"] == session_id:
        if force:
            print("NPSSI: 强制结束: 正在关闭应用 " + tstruct["Process"].app_name + "位于终端 " + terminal_id)
        else:
            print("NPSSI: 结束任务: 正在关闭应用 " + tstruct["Process"].app_name + "位于终端 " + terminal_id)

        tstruct["Process"].stop_task(force)
    pass

def StartApp(terminal_id: str):  # 启动app
    if terminal_id not in task_struct.keys():
        raise NoSuchTerminalError(terminal_id)

    tstruct = task_struct[terminal_id]
    print("NPSSI: 启动应用: 位于终端" + terminal_id + " 的应用已启动。")
    return tstruct["Process"].start_task()
    pass

def ListApp(refresh: bool=False):  #获取app列表。基本上等同于刷新。
    global applications
    if refresh:
        applications = load_config("NPSSI.Config")

    retval = {}
    for app in applications.keys():
        retval[app] = applications[app]["description"]

    SMBLib.UpdateAppListEvent(NPSSI, retval)
    return retval
    pass

def ListOpenedApps(search_by_sessions: Optional[list]=None):  # 列出正在运行的app
    rlist = []
    if search_by_sessions is None:
        for terminal_id in task_struct:
            #if task_struct[terminal_id]["Process"].is_running():
            rlist.append((
                    task_struct[terminal_id]["SessionID"],
                    terminal_id
                ))

    for session_id in search_by_sessions:
        for terminal_id in task_struct.keys():
            if task_struct[terminal_id]["SessionID"] == session_id:
                #if task_struct[terminal_id]["Process"].is_running():
                rlist.append((session_id, terminal_id))

    return rlist
    pass

def Shutdown():
    print("NPSSI: 正在关闭所有进程。")
    for terminal_id in task_struct.keys():
        task_struct[terminal_id]["Process"].stop_task(True)
    pass

# # 由此开始设计NPSSI的本地功能接口
#
# def create_process() -> str:
#     pass
#
# def start_process(terminal_id: str):
#     pass
#
# def terminate_process(terminal_id: str):
#     pass
#
# def kill_process(terminal_id: str):
#     pass


def ModuleInfo():
    return {
        "name": "Backend.NPSSI",
        "description": "本地进程输入输出流接口适配器",
        "version": "0.1",
        "deprecated": "0.0",
        "require": [
            ("LiveComFW", "0.1"),
            ("Services.MessageRouter", "0.2"),
            ("Services.SessionManager", "0.1")
        ]
    }


def Setup(_sysbus: EventBus):
    global SMBLib
    global APIClient_SMBLib
    global UMPLib
    global sysbus
    global broadcast_sender
    global broadcast_receiver
    global applications
    global task_struct
    global APIHost
    sysbus = _sysbus
    print("NPSSI: 初始化: 加载会话管理器后端工具库API")
    APIClient_SMBLib = sysbus.APIManager.load_library("SMBLib")
    SMBLib = APIClient_SMBLib.spawn_caller_module()

    print("NPSSI: 初始化: 注册 UMP 处理例程")
    UMPLib = APIUMPModuleUtils(_sysbus, "Backend.NPSSI", NPSSI)
    broadcast_receiver = UMPLib.receiver
    broadcast_sender = UMPLib.connector
    broadcast_receiver.register("StdInReceiver", data_handler)

    print("NPSSI: 初始化: 注册 NPSSI 回调工具API")
    APIHost = _sysbus.APIManager.create_library("NPSSI_Callback",
                                                _sysbus.EType.INCOMING,
                                                "NPSSIControlChannel",
                                                _sysbus.EType.OUTGOING,
                                                "NPSSIControlChannel")

    APIHost.register("OpenApp", OpenApp)
    APIHost.register("CloseApp", CloseApp)
    APIHost.register("StartApp", StartApp)
    APIHost.register("ListApp", ListApp)
    APIHost.register("ListOpenedApps", ListOpenedApps)
    APIHost.register("Shutdown", Shutdown)

    print("NPSSI: 初始化: 注册 NPSSI 回调工具API到会话管理器")
    SMBLib.RegisterBackend(NPSSI, "NPSSI_Callback")

    ListApp(refresh=True)
    pass

def Takedown(sysbus):
    print("NPSSI: 反初始化: 正关闭正在运行的所有应用实例")
    Shutdown()
    print("NPSSI: 反初始化: 解除到会话管理器的回调注册")
    broadcast_receiver.unregister("StdInReceiver")
    print("NPSSI: 反初始化: 反注册 UMP 处理例程")
    SMBLib.UnregisterBackend(NPSSI)
    print("NPSSI: 反初始化: 已完成")
    pass