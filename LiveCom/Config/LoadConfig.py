"""
This file is used to load config file in some python calls.
DO NOT REMOVE THIS FILE, or LiveCom cannot work properly.
"""
import sys
import os
import json
from typing import Union, Dict, List

# always have some interesting ways to accomplish works.
startup_path = sys._getframe().f_code.co_filename

if os.path.sep not in startup_path:
    # this is directly run by shell, not called by someone.
    raise RuntimeError("LoadConfig code should not eval directly in shell.")
    pass

config_path = os.path.split(startup_path)[0]

# Init done.


def _get_filename(cfgpath: str) -> str:
    return os.path.normpath(config_path + os.path.sep + cfgpath.replace('.', os.path.sep) + ".json")


def load_config(cfgpath: str) -> Union[Dict, List]:
    filename = _get_filename(cfgpath)
    print("LiveCom: 配置文件管理器: 正在读取配置文件: " + cfgpath)
    fobj = open(filename, mode="r", encoding="UTF-8")
    try:
        return json.load(fobj)
    finally:
        fobj.close()


def save_config(cfgpath: str, context: Union[Dict, List], *, part_update_only: bool=False) -> int:
    if part_update_only:
        print("LiveCom: 配置文件管理器: 正在更新配置文件: " + cfgpath)
    else:
        print("LiveCom: 配置文件管理器: 正在写入配置文件: " + cfgpath)
    filename=_get_filename(cfgpath)
    if part_update_only:
        src_context = load_config(cfgpath)
        if type(src_context) is not type(context):
            raise TypeError("Mismatched type between original config file.")
        if type(src_context) is list:
            src_context.extend(context)
        elif type(src_context) is dict:
            src_context.update(context)
        else:
            raise TypeError("Unsupported context type", type(src_context))
        context = src_context

    # write context to file.
    fobj = open(filename, mode="w", encoding="UTF-8")
    try:
        return json.dump(context, fobj, ensure_ascii=False, indent=4)
    finally:
        fobj.close()


def check_config_existence(cfgname: str) -> bool:
    filename = _get_filename(cfgname)
    return os.path.exists(filename) and not os.path.isdir(filename)


def remove_config(cfgpath: str):
    print("LiveCom: 配置文件管理器: 正在删除配置文件: " + cfgpath)
    return os.remove(_get_filename(cfgpath))

