import LiveCom.Auxiliaries.CQHTTP.cqhttp_msgobj_tools as msgtool

testvals = {
    "CQHTTP:@:U12345678": {
        'post_type': 'message',
        'partial': True,
        'message_type': 'private',
        'user_id': '12345678'
    },
    "CQHTTP:@F:U12345678": {
        'post_type': 'message',
        'partial': False,
        'message_type': 'private',
        'sub_type': 'friend',
        'user_id': '12345678'
    },
    "CQHTTP:@G:U12345678": {
        'post_type': 'message',
        'partial': False,
        'message_type': 'private',
        'sub_type': 'group',
        'user_id': '12345678'
    },
    "CQHTTP:@D:U12345678": {
        'post_type': 'message',
        'partial': False,
        'message_type': 'private',
        'sub_type': 'discuss',
        'user_id': '12345678'
    },
    "CQHTTP:G12345678:U12345678": {
        'post_type': 'message',
        'partial': False,
        'message_type': 'group',
        'group_id': '12345678',
        'sub_type': 'normal',
        'user_id': '12345678'
    },
    'CQHTTP:G12345678:A"text-text-text"': {
        'post_type': 'message',
        'partial': False,
        'message_type': 'group',
        'group_id': '12345678',
        'sub_type': 'anonymous',
        'anonymous': 'text-text-text'
    },
    "CQHTTP:G12345678:N": {
        'post_type': 'message',
        'partial': False,
        'message_type': 'group',
        'group_id': '12345678',
        'sub_type': 'notice'
    },
    "CQHTTP:G12345678": {
        'post_type': 'message',
        'partial': True,
        'message_type': 'group',
        'group_id': '12345678'
    },
    "CQHTTP:D12345678:U12345678": {
        'post_type': 'message',
        'partial': False,
        'message_type': 'discuss',
        'discuss_id': '12345678',
        'user_id': '12345678'
    },
    "CQHTTP:D12345678": {
        'post_type': 'message',
        'partial': True,
        'message_type': 'discuss',
        'discuss_id': '12345678'
    },

}

for item, result in testvals.items():
    if not msgtool.msgobj_paraser(item) == result:
        assert "Content not match. item " + item + \
               "\nexpecting: " + str(result) + "\n      got:" + str(msgtool.msgobj_paraser(item))
print("No error found.")



# refernce format:
# CQHTTP:@F:U12345678
# CQHTTP:@G:U12345678
# CQHTTP:@D:U12345678
# CQHTTP:G12345678:U12345678
# CQHTTP:G12345678:A"text-text-text"
# CQHTTP:G12345678:N
# CQHTTP:D12345678:U12345678

# get example
# ins = 'CQHTTP:D12345678'
# print(
#     '\t"' + ins + '": '  + \
#     str(msgtool.msgobj_paraser(ins)).replace("{", "{\n\t\t").replace(", ", ",\n\t\t").replace("}", "\n\t},")
# )
