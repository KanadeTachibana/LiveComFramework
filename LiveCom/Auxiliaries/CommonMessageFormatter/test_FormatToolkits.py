from LiveCom.Auxiliaries.CommonMessageFormatter.FormatToolkits import pack, unpack

texts = '测试文字\\[转义框选请无视\\]测试文字[type="value", key="value2", text="内部文字[引号内框选需要无视]内部文字"]测试文字[type="test2", data="内部文字\\"转义引号别当成引号结束了，另外转义引号没有成对的要求"]测试文字\\[转义框在哪里都有可能，只要在框选内的引号外面就请无视\\]测试文字"测试外部引号[type="test3", info="这个框选不可无视"]测试外部引号\\[type="test4", warn="这不是有效的框选。\\]我想这个测试足够变态但也足够说明我的需求了。'

print("Unpack:")
print(str(unpack(texts)))
print("Repack:")
print(pack(unpack(texts)))
print("Origional text:")
print(texts)
if pack(unpack(texts)) != texts:
    print("Check Failed!")
    raise ValueError()
else:
    print("Check OK!")
