class EType(object):
    """
    EType: EventType, indicate two of the queues.

    INCOMING for incoming receivers, such as message processor;
    OUTGOING for outgoing senders, such as messageing adaptors.
    """
    INCOMING = "incoming"
    OUTGOING = "outgoing"