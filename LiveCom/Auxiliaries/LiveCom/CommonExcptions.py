class NotAvailableError(Exception):
    def __init__(self, myself: object, extrainfo: object=None):
        self.myself = myself
        self.extra_info = extrainfo

    def __str__(self):
        if self.extra_info is not None:
            return "This " + str(self.myself.__class__.__name__) + " object is no longer available: " + (
                str(self.extra_info) if "__str__" in dir(self.extra_info) else self.extra_info.__repr__()
            )
        return "This " + str(self.myself.__class__.__name__) + " object is no longer available."
