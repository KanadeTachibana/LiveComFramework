from typing import Optional, Union, List, Iterable, Tuple


def CreateSession(user_terminal_target: str, app_name: str=None) -> str:
    return ""
    pass


def CreateProcess(session_name: str, app_name: str, arg_info: dict=None) -> str:
    return ""
    pass


def CreateProcessAndWaitForReturn(session_name: str, app_name: str, arg_info: dict=None, timeout: int=-1) -> int:
    return 0
    pass


def WaitForReturnValue(session_name: str, terminal_id: str, timeout: int=-1) -> int:
    return 0
    pass


def DestorySession(session_name: str):
    pass


def DestoryProcess(session_name: Optional[str], terminal_id: str, force=False):
    pass


def ListTask(backend_codename: Optional[Union[str, Iterable]]=None,
             session_name: Optional[Union[str, Iterable]]=None) -> List[Tuple[str]]:
    return []
    pass


def ListSession() -> dict:
    return {}
    pass


def ListApps(backend_codename: str=None) -> List[Tuple[str]]:
    return []
    pass
