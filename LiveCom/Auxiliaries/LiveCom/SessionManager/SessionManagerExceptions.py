#from LiveCom.Auxiliaries.LiveCom.SessionManager.SessionManagerUtils import generate_hexstring

class DuplicatedBackendCodenameError(Exception):
    def __init__(self, backend_codename: str, backend_libname: str):
        self.backend_codename = backend_codename
        self.backend_libname = backend_libname

    def __str__(self):
        return "Duplicated backend codename '" + self.backend_codename + \
            "' for backend which have library name '" + self.backend_libname + "'."

class NoSuchBackendCodenameError(Exception):
    def __init__(self, backend_codename: str):
        self.backend_codename = backend_codename

    def __str__(self):
        return "No such backend codename: " + self.backend_codename

class NoSuchSessionError(Exception):
    def __init__(self, session_id):
        self.session_id = session_id
        self.session_name = _generate_hexstring(session_id, 8)

    def __str__(self):
        return "No such session: " + self.session_name + \
               " (" + str(self.session_id) + ")"

class NoSuchTerminalError(Exception):
    def __init__(self, session_id: int, terminal_id: str):
        self.session_id = session_id
        self.session_name = _generate_hexstring(session_id)
        self.terminal_id = terminal_id

    def __str__(self):
        return "No such backend process terminal '" + self.terminal_id + \
               "' available in session '" + self.session_name + "'."

class NoClosableProcessAvailableError(Exception):
    def __init__(self, terminal_id: str):
        self.terminal_id = terminal_id

    def __str__(self):
        return "No closable process for specified terminal name:" + self.terminal_id

class NoSuchAppError(Exception):
    def __init__(self, app_name):
        self.app_name = app_name

    def __str__(self):
        return "Requested app is not registered in NPSSI: " + self.app_name

def _generate_hexstring(number: int, lenth: int=8):
    if number < 0:
        raise NotImplementedError()
    return format(number, str(lenth)+"x").replace(" ", "0").upper()