from LiveCom.Auxiliaries.LiveCom.NPSSI.ObjectBuffers import StreamObjectBuffer

from time import sleep


def dummy(data):
    for line in data:
        print(line)

s = StreamObjectBuffer(dummy, alarm_time=0.5)

for i in range(5):
    s.add_content(str(i))

sleep(1)

for i in range(30):
    s.add_content(str(i))
    sleep(0.05)
