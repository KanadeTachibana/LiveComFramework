from LiveCom.SystemComponents.EventBus import EventBus
from LiveCom.Auxiliaries.LiveCom.API.APICommonExceptions import *


class APIUMPModuleUtils(object):
    def __init__(self, sysbus: EventBus, adaptor_name: str, adaptor_codename: str):
        self.adaptor_codename = adaptor_codename
        self.adaptor_name = adaptor_name
        self._sysbus = sysbus
        if sysbus.is_sysbus:
            # will register itself.
            if adaptor_name not in \
                    sysbus.APIManager.APILoader._available_module_table.keys():
                raise NoSuchModuleError(module_name=adaptor_name)

            try:
                if adaptor_name in sysbus.APIManager.user_extra_data["NameResolverUtils"]["AdaptorCodenameTable"]:
                    raise DuplicatedUMPObjectError(module_name=adaptor_name)
            except KeyError:
                try:
                    if "AdaptorCodenameTable" not in sysbus.APIManager.user_extra_data["NameResolverUtils"]:
                        sysbus.APIManager.user_extra_data["NameResolverUtils"]["AdaptorCodenameTable"] = dict()
                except KeyError:
                    sysbus.APIManager.user_extra_data["NameResolverUtils"]["AdaptorCodenameTable"] = dict()

            sysbus.APIManager.user_extra_data["NameResolverUtils"]["AdaptorCodenameTable"][adaptor_name] \
                = adaptor_codename

            # start init broadcast objects
            self.receiver = sysbus.APIManager.create_broadcast_receiver(
                sysbus.EType.OUTGOING,
                "UnifiedMessageProcessors", "UMP: " + self.adaptor_name)

            self.connector = sysbus.APIManager.create_brodacast_connecter(
                "UMP: " + self.adaptor_name, adaptor_name,
                sysbus.EType.INCOMING, "UnifiedMessageProcessors",
                allow_invaild_target=True)
            return

    def get_controls(self) -> tuple:
        return self.receiver, self.connector

    def __del__(self):
        # unregister all thing that have registered.
        # leave receiver and connector alone: GC will handle this.
        self._sysbus.APIManager.user_extra_data["NameResolverUtils"] \
            ["AdaptorCodenameTable"].pop(self.adaptor_name)
        pass
