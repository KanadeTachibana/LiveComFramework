class NoSuchModuleError(Exception):
    def __init__(self, module_name: str):
        self.module_name = module_name

    def __str__(self):
        return "No such module in available module list: " + self.module_name


class DestroyLibraryInUse(Exception):
    def __init__(self, library_name: str, remaining_ref_count: int):
        self.library_name = library_name
        self.remaining_ref_count = remaining_ref_count

    def __str__(self):
        return "Destroying in-use library '" + self.library_name + \
               "', library reference count is not zero. (Remaining reference(s): " + str(self.remaining_ref_count) + ")"


class GroupNotFoundError(Exception):
    def __init__(self, group_etype: str, group_name, target_type: str=None):
        self.target_type = target_type
        self.group_etype = group_etype
        self.group_name = group_name

    def __str__(self):
        if self.target_type is None:
            return "Requested group not found: EType: '" + \
                   self.group_etype + "', Name: '" + self.group_name + "'."
        return "Requested " + self.target_type + " target not found: EType: '" + \
            self.group_etype + "', Name: '" + self.group_name + "'."


class DuplicatedBroadcastReceiverError(Exception):
    def __init__(self, cast_name: str):
        self.cast_name = cast_name

    def __str__(self):
        return "Duplicated broadcast receiver name: " + self.cast_name


class DuplicatedBroadcastConnectorError(Exception):
    def __init__(self, sender_name: str):
        self.sender_name = sender_name

    def __str__(self):
        return "Duplicated broadcast receiver name: " + self.sender_name


class DuplicatedLibraryError(Exception):
    def __init__(self, library_name: str):
        self.library_name = library_name

    def __str__(self):
        return "Duplicated library name: " + self.library_name


class DuplicatedFunctionError(Exception):
    def __init__(self, function_name: str, library_name: str):
        self.function_name = function_name
        self.library_name = library_name

    def __str__(self):
        return "Duplicated function in library '" + self.library_name + "': " + self.function_name


class NoSuchLibraryError(Exception):
    def __init__(self, library_name: str):
        self.library_name = library_name

    def __str__(self):
        return "No such library '" + self.library_name + "'."


class NoSuchFunctionError(Exception):
    def __init__(self, function_name: str, library_name: str):
        self.function_name = function_name
        self.library_name = library_name

    def __str__(self):
        return "No such function '" + self.function_name + "' in library '" + self.library_name + "'."


class DuplicatedUMPObjectError(Exception):
    def __init__(self, module_name: str):
        self.module_name = module_name

    def __str__(self):
        return "Duplicated UMP object for module '" \
               + self.module_name + \
               "', every module should only have one UMP object."


class ModuleDependencyError(Exception):
    # case Version mismatch: ("version", (module_name, expecting, got))
    # case No available module: ("no_available", (module_name, module_version))
    # case Deprecated support: ("deprecated", (module_name, requesting, dropped_since))
    def __init__(self, reason: str, info: tuple):
        self.reason = reason
        if reason == "version":  # version mismatch
            self.module_name, self.ver_expecting, self.ver_got = info
        elif reason == "no_available":
            self.module_name, self.module_ver = info
        elif reason == "deprecated": # deprecated support
            self.module_name, self.ver_expecting, self.deprecated_since = info
        else:
            raise ValueError("Unsupported reason type: " + reason)

        self.info_tuple = info

    def __str__(self):
        if self.reason == "version":
            return "Dependency not satisfied due to version requirement: Expecting module '" \
                   + self.module_name + "' version '" + self.ver_expecting + "', but got version '" \
                    + self.ver_got + "'."
        elif self.reason == "no_available":
            return "Dependency not satisfied due to required module not found: Expecting module '" \
                   + self.module_name + "' version '" + self.module_ver + "'."
        elif self.reason == "deprecated":
            return "Dependency not satisfied due to deprecated support: Expecting module '" \
                   + self.module_name + "' version '" + self.ver_expecting + "', but since version '" \
                   + self.deprecated_since + "', all older version supports are deprecated."
    pass


class ModuleFormatError(Exception):
    pass


class APIManagerError(Exception):
    pass