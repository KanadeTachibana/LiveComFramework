# 名称解析服务

在LiveCom框架中，名称解析服务是用于解析各个容器以及容器内的各个成员的显示名称，以提供给前后端适配器进行信息显示。

## 名称解析函数与注册名称解析函数

各个适配器应当注册自身所提供的一个函数用于提供给名称解析服务实施解析动作。

### 注册对名称解析服务的依赖
在使用名称解析服务注册名称解析函数之前，应当在模块信息中指明要使用名称解析服务。

> 名称解析服务的模块名为: `Services.NameResolver`

### 注册与解除注册解析函数

从API管理器中获得库 `NameResovler` ，该库中的函数[`register_resolver`](#注册解析函数)可以用于注册解析服务函数；[`unregister_resolver`](#反注册解析函数)可以用于解除先前注册的解析函数的注册。

有关“API管理器”的使用方式，请参见`APIManager.md`。

### 解析函数调用约定
解析函数需要以特定的调用约定进行构造。

**示例:**
```python
def your_resolver(msgobj: str) -> Optional[tuple]:
    # your code here...
    if success:  # we got a name.
        return "Platform name", "His container", "His name"
    else:  # failed to get a name.
        return None
```

解析器名称 `your_resolver` 可自行指定。
参数 `msgobj` 也可根据自身需要更换名称，在调用时将会使用顺序参数。

返回值分为两种情况。
 - 在成功解析出名称时，返回一个包含3个元素的元组，按顺序分别返回：**平台通用名称**、**容器名称**、**用户名称**。如果用户名不存在，用`None`表示。需要注意的是，平台名称和容器不得缺失，如果是特殊容器（以@开头的），可直接将容器名称设置为特殊容器的目的名称（例如：好友列表）。
 - 在未能解析名称时，直接返回`None`。解析处理函数在接到此值时会尝试下一个可用的解析器。如果全部解析器都失败，向调用者返回`None`。

### 解析优先级顺序

解析的优先级根据以下规则确定:

1. 注册解析函数时提供的优先级数值。
    当数值越大，优先级越高。
2. 优先级相同时，根据注册先后确定优先级。
    先注册的解析函数具备更高的优先级。

### 解析名称
从API管理器中获得库 NameResovler ，该库中的函数[`resolve`](#进行名称解析)可以用于解析消息目标对象并得到**平台通用名称**、**容器名称**、**用户名称**（如果有）。

## API接口

### 注册解析函数
```python
register_resolver(module_name: str, module_codename: str, handler: Callable, *, priority: int=1024) -> None
```

**参数**:
`module_name`: _**str**_: 注册的模块名称。该参数影响解析优先级。如果已有一个相同名称的解析器，则覆盖之前的设置。
`module_codename`: _**str**_: 适配器模块代号，用于识别特定的一个适配器类型。
`handler`: _**Callable**_: 处理函数。此处理函数对参数与返回值有特殊要求。详情请参见[解析函数调用约定](#解析函数调用约定)。
`priority`: _**int**_: 解析器优先级。默认是1024。数值越大，优先级越高。

**异常**: (无)

**返回**: (无)

### 反注册解析函数
```python
unregister_resolver(module_name: str, module_codename: Optional[str]=None) -> None
```

**参数**:
`module_name`: _**str**_: 已注册的模块名称。该参数影响解析优先级。
`module_codename`: _**Optional\[str\]**_: 适配器模块代号，用于识别特定的一个适配器类型。如果不指定或指定为`None`，将会解除所有适配器模块代号下所有相同模块名称的注册。

**异常**:
`NoSuchNameError`: 当指定的模块名称不存在时，将会抛出此异常。
`NoSuchCodenameError`: 当指定的模块代号不存在时，将会抛出此异常。

**返回**: (无)

### 进行名称解析
```python
resolve(msgobj: str) -> Optional[tuple]
```

**参数**:
`msgobj`: _**str**_: 要解析的消息目标对象，该对象为str类型。消息目标对象可以是一个具体成员目标，也可以是一个容器目标。

**异常**: (无)

**返回**: _**Optional\[tuple\]**_: 一个包含3个元素的元组，或者为None。

当解析成功时，返回的元组内的内容按顺序为：**平台通用名称**、**容器名称**、**用户名称**。如果用户名称不存在，此项在元组中的对应位置为`None`。

如果在所有注册的解析函数中都未能成功解析，或没有对应的解析函数可用，将返回`None`。

> **返回值格式**:
>
> (Return Case: 成功解析): _**tuple**_
> - [0]: _**str**_: 平台通用名称
> - [1]: _**str**_: 容器名称
> - [2]: _**Optional\[str\]**_: 用户名称（如果有），或`None`（如果没有）。
>
> (Return Case: 解析失败): _**None**_

### 由模块代号获得模块名称
```python
get_module_name_by_codename(codename: str) -> List[str]
```

此函数返回的列表的信息是从由 `LiveCom.Auxiliaries.LiveCom.API.APIToolkits` 的 `APIUMPModuleUtils` 类进行维护的，位于API管理器中的数据结构中取得的。  
只有通过`APIUMPModuleUtils` 创建了广播收发器的UMP适配器模块才会在本函数内被找到。

**参数**:  
`codename`: _**str**_: 模块代号

**异常**: (无)

**返回**: _**List[str]**_：模块名称的列表。如果没有任何一个模块，返回的将会是空列表。

> **返回值格式**:  
> 
> (Return): _**list**_
>  - Values: _**str**_: 模块名称