# example context format
incoming_context={
    "adaptor": "ADAPTOR.NAME", # 适配器名称（也就是注册模块时的名称）
    "source": "INTERFACE:CONTAINER:USER",  # 标记来源
    "message": "inline[type=\"format\", context=\"1\"]of text", # 消息文本，使用内联消息格式。
    "platform_context": {
        # 平台相关上下文信息
    },
    "routing_path": []  # 适配器应当原样转发这个字段，用于路径分析。
}

outgoing_context={
    "target": "INTERFACE:CONTAINER:USER",  # 标记目标
    "message": "inline[type=\"format\", context=\"1\"]of text", # 消息文本，使用内联消息格式。
    "routing_path": [  # 消息路由器负责将前一个消息的信息加入这里。[0]始终是当前信息。[1]是前一跳的信息。[-1]是初始消息。
        {
            "adaptor": "ADAPTOR.NAME",
            "source": "INTERFACE:CONTAINER:USER",
            "message" "data data data..."
            "target": "INTERFACE:CONTAINER:USER",
            "platform_context": {
                # 平台相关上下文信息
            }
        }
    ]
}