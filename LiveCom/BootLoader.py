"""
Bootloader for LiveCom.
This file is used to setup basic environment of LiveCom framework.
"""
from LiveCom.Auxiliaries.LiveCom import LoadAdaptors, SystemInfo
from threading import Thread
from LiveCom.Auxiliaries.LiveCom.SystemInfo import getSystemVersion

SystemInfo.showLogo()

print("LiveCom: 初始化: LiveCom 即时聊天服务平台  版本 " + getSystemVersion() + " 正在启动。")
# create system message bus
from LiveCom.SystemComponents.EventBus import EventBus

SystemBus = EventBus(name="LiveComSystemBus", system_bus=True)

def server_thread_starter():
    SystemBus.start_loop()


# create server thread
server_thread = Thread(target=server_thread_starter, name="Server Thread")
server_thread.start()

from LiveCom.Adaptors import CommonInterfaceSetup

# Common interface setup
#CommonInterfaceSetup.setupCommonGroup(SystemBus)
# Register adaptors
SystemBus.APIManager.APILoader._system_startup()
try:
    print("LiveCom: 初始化: 初始化完成。")
    server_thread.join()
except KeyboardInterrupt:
    SystemBus.APIManager.APILoader._system_shutdown()
